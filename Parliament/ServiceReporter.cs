﻿/********************************************************************************************/
using System.IO;
using Parliament.Service;
using Parliament.Dto;

using Microsoft.Practices.Unity;
/********************************************************************************************/


namespace Parliament
{
    class ServiceReporter
    {
        private IUnityContainer unityContainer;
        private TextWriter output;


/*------------------------------------------------------------------------------------------*/

        public ServiceReporter( IUnityContainer _unityContainer, TextWriter _output )
        {
            this.unityContainer = _unityContainer;
            this.output = _output;
        }

/*------------------------------------------------------------------------------------------*/

            public void GenerateReports ()
        {
            ReportAccountCollection( "Administrators", unityContainer.Resolve< IAdministratorService >() );
            ReportAccountCollection( "Users", unityContainer.Resolve< IUserService >() );
            ReportCollection( "Laws", unityContainer.Resolve< ILawService >() );
            ReportCollection( "Bills", unityContainer.Resolve< IBillService >() );
        }

/*------------------------------------------------------------------------------------------*/

        private void ReportCollection< T > ( string title, IDocumentService< T > service )
            where T : DocumentDto
        {
            output.WriteLine( "==== {0} ==== ", title );
            output.WriteLine();

            foreach ( var entityId in service.ViewAll() )
            {
                // TODO: Unexpected error. There is a cap for current service.
                // Instead of current behaviour there should be: service.ViewFullInformation( entityId ).

                output.Write( service.ViewFullInformation( entityId ) );

                output.WriteLine();
            }

            output.WriteLine();
        }

/*------------------------------------------------------------------------------------------*/

        private void ReportAccountCollection< T > ( string title, IAccountService< T > service )
            where T : AccountDto
        {
            output.WriteLine( "==== {0} ==== ", title );
            output.WriteLine();

            foreach ( var entityId in service.ViewAll() )
            {
                output.Write( service.View( entityId) );

                output.WriteLine();
            }

            output.WriteLine();
        }
    }
}


/********************************************************************************************/
