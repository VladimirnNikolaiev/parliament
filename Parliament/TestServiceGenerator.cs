﻿/********************************************************************************************/
using System;
using Parliament.Service;
using Parliament.Model;
using System.Collections.Generic;

using Microsoft.Practices.Unity;
/********************************************************************************************/

namespace Parliament
{
    class TestServiceGenerator
    {
        private IUnityContainer unityContainer;

        private Guid AdminStepan, AdminTaras, AdminDmitro;
        private Guid Lyashco, Barna, Parubiy, Novinskiy;
        private Guid RegUser1, UnregUser1;
        private Guid EconomicBill;
        private Guid SocialPolicyLaw;

/*------------------------------------------------------------------------------------------*/

        public TestServiceGenerator( IUnityContainer _unityContainer )
        {
            this.unityContainer = _unityContainer;
        }

        public void GenerateTestData()
        {
            GenerateAdmins();
            GenerateUsers();

            GenerateRegisteredUnregisteredUsers();

            //TODO: unexpected error after using method ViewFullInformation() in ServiceReporter.cs
            GenerateLaw();
            GenerateBill();
        }

 /*------------------------------------------------------------------------------------------*/

        private void GenerateAdmins()
        {
            var adminService = unityContainer.Resolve< IAdministratorService >();

            AdminStepan = adminService.createNew(
                    "stepan.kovtun@gov.ua"
                ,   "1111"
                ,   "Stepan Kovtun"
                ,   "DocumentManagement"
                ,   "images/Admins/Kovtun.jpg"
            );

            AdminTaras = adminService.createNew(
                    "taras.koval@gov.ua"
                 ,   "2222"
                 ,   "Taras Koval"
                 ,   Administrator.AdministratorAccessRight.AccountsManagement.ToString()
                 ,   "images/Admins/Koval.jpg"
           );

            AdminDmitro = adminService.createNew(
                    "Dmitrii.Rostov@gov.ua"
                ,   "3333"
                ,   "Dmitrii Rostov"
                ,   Administrator.AdministratorAccessRight.Full.ToString()
                ,   "images/Admins/Rostov.jpg"
            );

            // NOTE: Uncomment to see how exception works.
            // Copied from previous account.

            //Guid BadAdmin = adminService.createNew(
            //        "Dmitrii.Rostov@gov.ua"
            //    ,   "3333"
            //    ,   "Dmitrii Rostov"
            //    ,   Administrator.AdministratorAccessRight.Full.ToString()
            //);

        }

/*------------------------------------------------------------------------------------------*/

        private void GenerateUsers()
        {
            var userService = unityContainer.Resolve< IUserService >();

            Lyashco = userService.createNew (
                    "oleg.lyashco@gov.ua"
               ,    "Skotyniaky"
               ,    "Oleg Lyashco"
               ,    "Deputy"
               ,   "images/Users/Lyashko.jpg"
            );


           Barna = userService.createNew (
                    "oleg.barna@gov.ua"
               ,    "YatcenykaDoloy"
               ,    "Oleg Barna"
               ,    "Deputy"
               ,   "images/Users/Barna.jpg"
            );

           Parubiy = userService.createNew (
                    "andriy.parubiy@gov.ua"
               ,    "2222"
               ,    "Andriy Parubiy"
               ,    "Speaker"
               ,   "images/Users/Parubiy.jpg"
            );

           Novinskiy = userService.createNew (
                    "vadim.novinskiy@gov.ua"
                ,   "1937"
                ,   "Vadim Novinskiy"
                ,   "Speaker"
                ,   "images/Users/Novinskiy.jpg"
            );

            // NOTE: Uncomment to see how exception works.
            // Copied from previous account.

            //Guid badDeputy = userService.createNew(
            //        "vadim.novinskiy@gov.ua"
            //    ,   "1937"
            //    ,   "Vadim Novinskiy"
            //    ,   "Speaker"
            //);
        }

/*------------------------------------------------------------------------------------------*/

        private void GenerateRegisteredUnregisteredUsers()
        {
            var registeredDCService = unityContainer.Resolve< IRegisteredDocumentCreatorService >();
            var unregisteredDCService = unityContainer.Resolve< IUnregisteredDocumentCreatorService >();
            var userService = unityContainer.Resolve< IUserService >();

            RegUser1 = registeredDCService.addNew( Lyashco );

            UnregUser1 = unregisteredDCService.addNew(
                   "Petro"
                ,  "President"
            );
        }

/*------------------------------------------------------------------------------------------*/

        // TODO: Generate more laws.
        // Use Internet to help with examples.

        private void GenerateLaw()
        {
            var lawService = unityContainer.Resolve< ILawService >();
            var unregisteredDCService = unityContainer.Resolve<IUnregisteredDocumentCreatorService>();

            var creators = new List<Guid>();
            creators.Add( UnregUser1 );
            SocialPolicyLaw = lawService.addNew(
                        "SocialPolicyLaw"
                    ,   Law.LawsStatus.Act.ToString()
                    ,   @"..\..\..\Parliament.Model\TestResources\SocialPolicyLaw.txt"
                    ,   new DateTime( 2014, 06, 13 )
                    ,   creators
            );

            unregisteredDCService.addLawToCreator( UnregUser1, SocialPolicyLaw );
        }


/*------------------------------------------------------------------------------------------*/

        // TODO: Generate more bills.
        // Use Internet to help with examples.

        private void GenerateBill()
        {
            var billService = unityContainer.Resolve< IBillService >();
            var registeredDCService = unityContainer.Resolve<IRegisteredDocumentCreatorService>();

            var creators = new List<Guid>();
            creators.Add( RegUser1 );
            EconomicBill = billService.addNew (
                        "EconomicBill"
                    ,   Bill.BillsStatus.New.ToString()
                    ,   @"..\..\..\Parliament.Model\TestResources\EconomicBill.txt"
                    ,   new DateTime( 2016, 10, 05 )
                    ,   creators
                );

            registeredDCService.addBillToCreator( RegUser1, EconomicBill );
        }
    }
}


/********************************************************************************************/