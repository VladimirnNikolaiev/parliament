﻿/********************************************************************************************/
using System;
using System.Linq;
using Parliament.Model;
/********************************************************************************************/


namespace Parliament
{
    class TestModelGenerator
    {
        public void GenerateTestData ( ParliamentModel _pm )
        {
            Test t = new Test();

            GenerateAccounts( _pm );
            GenerateSessions( _pm );
            GenerateRegisteredUnregisteredUsers( _pm );
            GenerateBill( _pm );
            GenerateLaw( _pm );
            t.StartModelTest( _pm );
        }

/*------------------------------------------------------------------------------------------*/

        private void GenerateAccounts ( ParliamentModel _pm )
        {
            Administrator Stepan = new Administrator (
                    Guid.NewGuid()
                ,   "Stepan Kovtun"
                ,   "1111"
                ,   "stepan.kovtun@gov.ua"
                ,   Account.AccountStatus.Act
                ,   Administrator.AdministratorAccessRight.AccountsManagement
                ,   "images/Admins/Kovtun.jpg"
            );

            User Lyashco = new User (
                    Guid.NewGuid()
                ,   "Oleg Lyashco"
                ,   "Skotyniaky"
                ,   "oleg.lyashco@gov.ua"
                ,   Account.AccountStatus.Act
                ,   User.UserPost.Deputy
                ,   "images/Users/Lyashko.jpg"
            );

            User Barna = new User (
                    Guid.NewGuid()
                ,   "Oleg Barna"
                ,   "YatcenykaDoloy"
                ,   "oleg.barna@gov.ua"
                ,   Account.AccountStatus.Act
                ,   User.UserPost.Deputy
                ,   "images/Users/Barna.jpg"
            );

            User Parubiy = new User (
                    Guid.NewGuid()
                ,   "Andriy Parubiy"
                ,   "2222"
                ,   "andriy.parubiy@gov.ua"
                ,   Account.AccountStatus.Act
                ,   User.UserPost.Speaker
                ,   "images/Users/Parubiy.jpg"
            );

            _pm.Administrators.Add( Stepan );
            _pm.Users.Add( Lyashco );
            _pm.Users.Add( Barna );
            _pm.Users.Add( Parubiy );
          }

/*------------------------------------------------------------------------------------------*/

        private void GenerateSessions ( ParliamentModel _pm )
        {
            for ( int i = 0; i < _pm.Administrators.Count; ++i )
                _pm.Sessions.Add( new Session( Guid.NewGuid(), _pm.Administrators.ElementAt( i ) ) );

            for ( int i = 0; i < _pm.Users.Count; ++i )
                _pm.Sessions.Add( new Session( Guid.NewGuid(), _pm.Users.ElementAt( i ) ) );
        }

/*------------------------------------------------------------------------------------------*/

        private void GenerateRegisteredUnregisteredUsers ( ParliamentModel _pm )
        {
            RegisteredDocumentCreator rdc1 = new RegisteredDocumentCreator (
                    _pm.Users.ElementAt( 1 ).DomainId
                ,   ( User )_pm.Users.ElementAt( 1 )
            );

            RegisteredDocumentCreator rdc2 = new RegisteredDocumentCreator (
                    _pm.Users.ElementAt( 0 ).DomainId
                ,   ( User )_pm.Users.ElementAt( 0 )
            );

            UnregisteredDocumentCreator urdc1 = new UnregisteredDocumentCreator (
                    Guid.NewGuid()
                ,   "Pavlo Petrenko"
                ,   UnregisteredDocumentCreator.UnregDCRole.MinisterOfSocialPolicy
            );

            _pm.RDocumentCreators.Add( rdc1 );
            _pm.RDocumentCreators.Add( rdc2 );
            _pm.UDocumentCreators.Add( urdc1 );
        }

/*------------------------------------------------------------------------------------------*/

        private void GenerateLaw ( ParliamentModel _pm )
        {
            DocumentInfo SocialPolicyInfo = new DocumentInfo (
                @"..\..\..\Parliament.Model\TestResources\SocialPolicyLaw.txt"
            );

            Revision SocialPolicyRevision = new Revision (
                    Guid.NewGuid()
                ,   new DateTime( 2014, 06, 13 )
                ,   SocialPolicyInfo
            );

            _pm.Revisions.Add( SocialPolicyRevision );

            Law SocialPolicyLaw = new Law (
                    Guid.NewGuid()
                ,   "SocialPolicyLaw"
                ,   Law.LawsStatus.Act
                ,   SocialPolicyRevision
            );

            _pm.Laws.Add( SocialPolicyLaw );
        }

/*------------------------------------------------------------------------------------------*/

        private void GenerateBill ( ParliamentModel _pm )
        {
            DocumentInfo EconomicInfo = new DocumentInfo (
                @"..\..\..\Parliament.Model\TestResources\EconomicBill.txt"
            );

            Revision EconomicRevision = new Revision (
                    Guid.NewGuid()
                ,   new DateTime( 2016, 10, 05 )
                ,   EconomicInfo
            );

            _pm.Revisions.Add( EconomicRevision );

            Bill EconomicBill = new Bill (
                    Guid.NewGuid()
                ,   "EconomicBill"
                ,   Bill.BillsStatus.New
                ,   EconomicRevision
            );


            DocumentInfo CourtOfJusticeInfo = new DocumentInfo (
                @"..\..\..\Parliament.Model\TestResources\CourtOfJustice.txt"
           );

            Revision CourtOfJusticeRevision = new Revision (
                    Guid.NewGuid()
                ,   new DateTime( 2015, 03, 01 )
                ,   CourtOfJusticeInfo
            );

            _pm.Revisions.Add( CourtOfJusticeRevision );

            Bill CourtofJusticeBill = new Bill (
                    Guid.NewGuid()
                ,   "CourtOfJusticeBill"
                ,   Bill.BillsStatus.VotedIn3rdReading
                ,   CourtOfJusticeRevision
            );
                    
            _pm.Bills.Add( EconomicBill );
            _pm.Bills.Add( CourtofJusticeBill );
        }
    }
}


/********************************************************************************************/