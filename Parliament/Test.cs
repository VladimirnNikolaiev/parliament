﻿/********************************************************************************************/
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using Parliament.Model;
/********************************************************************************************/

namespace Parliament
{
    class Test
    {
        public void StartModelTest ( ParliamentModel _pm )
        {
            UsersTest( _pm );
            AdminTests( _pm );
            SessionsTests( _pm );
            RegistrTests( _pm );
            UnRegistrTests( _pm );
            LawTests( _pm );
            BillTests( _pm );
        }

/*------------------------------------------------------------------------------------------*/

        private void UsersTest ( ParliamentModel _pm )
        {
            Assert.AreEqual( _pm.Users.Count, 3 );
            
            Assert.AreEqual( _pm.Users.ElementAt( 0 ).Name, "Oleg Lyashco" );
            Assert.AreEqual( _pm.Users.ElementAt( 0 ).Password, "Skotyniaky" );
            Assert.AreEqual( _pm.Users.ElementAt( 0 ).Email, "oleg.lyashco@gov.ua" );
            Assert.AreEqual( _pm.Users.ElementAt( 0 ).Post, User.UserPost.Deputy );
                                                   
            Assert.AreEqual( _pm.Users.ElementAt( 1 ).Name, "Oleg Barna" );
            Assert.AreEqual( _pm.Users.ElementAt( 1 ).Password, "YatcenykaDoloy" );
            Assert.AreEqual( _pm.Users.ElementAt( 1 ).Email, "oleg.barna@gov.ua" );
            Assert.AreEqual( _pm.Users.ElementAt( 1 ).Post, User.UserPost.Deputy );
                                                   
            Assert.AreEqual( _pm.Users.ElementAt( 2 ).Name, "Andriy Parubiy" );
            Assert.AreEqual( _pm.Users.ElementAt( 2 ).Password, "2222" );
            Assert.AreEqual( _pm.Users.ElementAt( 2 ).Email, "andriy.parubiy@gov.ua" );
            Assert.AreEqual( _pm.Users.ElementAt( 2 ).Post, User.UserPost.Speaker );
        }

/*------------------------------------------------------------------------------------------*/

        private void AdminTests ( ParliamentModel _pm )
        {
            Assert.AreEqual( _pm.Administrators.Count, 1 );
                             
            Assert.AreEqual( _pm.Administrators.ElementAt( 0 ).Name, "Stepan Kovtun" );
            Assert.AreEqual( _pm.Administrators.ElementAt( 0 ).Password, "1111" );
            Assert.AreEqual( _pm.Administrators.ElementAt( 0 ).Email, "stepan.kovtun@gov.ua" );
            Assert.AreEqual( 
                    _pm.Administrators.ElementAt( 0 ).AccessRight
                ,   Administrator.AdministratorAccessRight.AccountsManagement
            );
        }

/*------------------------------------------------------------------------------------------*/

        private void SessionsTests ( ParliamentModel _pm )
        {
            Assert.AreEqual( _pm.Sessions.Count, 4 );

            Assert.AreEqual( _pm.Sessions.ElementAt( 0 ).Account.Name, _pm.Administrators.ElementAt( 0 ).Name );

            for ( int i = 0; i < _pm.Sessions.Count - 1; i++ )
            {
                if ( _pm.Sessions.ElementAt( i ).CloseTime == null)
                    Assert.AreEqual( _pm.Sessions.ElementAt( i ).Status, Session.SessionStatus.Opened );
                else
                    Assert.AreEqual( _pm.Sessions.ElementAt( i ).Status, Session.SessionStatus.Closed );
            }
        }

/*------------------------------------------------------------------------------------------*/

        private void RegistrTests( ParliamentModel _pm )
        {
            Assert.AreEqual( _pm.RDocumentCreators.Count, 2 );

            Assert.AreEqual( _pm.RDocumentCreators.ElementAt( 0 ).Account.Name, "Oleg Barna" );
            Assert.AreEqual( _pm.RDocumentCreators.ElementAt( 0 ).Account, _pm.Users.ElementAt( 1 ) );
                                                               
            Assert.AreEqual( _pm.RDocumentCreators.ElementAt( 1 ).Account.Name, "Oleg Lyashco" );
            Assert.AreEqual( _pm.RDocumentCreators.ElementAt( 1 ).Account, _pm.Users.ElementAt( 0 ) );
        }

/*------------------------------------------------------------------------------------------*/

        private void UnRegistrTests ( ParliamentModel _pm )
        {
            Assert.AreEqual( _pm.UDocumentCreators.Count, 1 );
            Assert.AreEqual( _pm.UDocumentCreators.ElementAt( 0 ).Name, "Pavlo Petrenko" );
            Assert.AreEqual( 
                    _pm.UDocumentCreators.ElementAt( 0 ).Role
                ,   UnregisteredDocumentCreator.UnregDCRole.MinisterOfSocialPolicy
            );
        }

/*------------------------------------------------------------------------------------------*/

        private void LawTests ( ParliamentModel _pm )
        {
            Assert.AreEqual( _pm.Laws.Count, 1);
                             
            Assert.AreEqual( _pm.Laws.ElementAt( 0 ).Name, "SocialPolicyLaw" );
            Assert.AreEqual( _pm.Laws.ElementAt( 0 ).Status, Law.LawsStatus.Act );
            Assert.AreEqual( _pm.Laws.ElementAt( 0 ).CreationDateAsString, "06.13.2014" );
            Assert.AreEqual( _pm.Laws.ElementAt( 0 ).DocumentText, "SocialPolicyLaw" );
        }

/*------------------------------------------------------------------------------------------*/

        private void BillTests ( ParliamentModel _pm )
        {
            Assert.AreEqual( _pm.Bills.Count, 2 );
                             
            Assert.AreEqual( _pm.Bills.ElementAt( 0 ).Name, "EconomicBill" );
            Assert.AreEqual( _pm.Bills.ElementAt( 0 ).Status, Bill.BillsStatus.New );
            Assert.AreEqual( _pm.Bills.ElementAt( 0 ).CreationDateAsString, "10.05.2016" );
            Assert.AreEqual( _pm.Bills.ElementAt( 0 ).DocumentText, "EconomicBill" );
                                                   
            Assert.AreEqual( _pm.Bills.ElementAt( 1 ).Name, "CourtOfJusticeBill" );
            Assert.AreEqual( _pm.Bills.ElementAt( 1 ).Status, Bill.BillsStatus.VotedIn3rdReading );
            Assert.AreEqual( _pm.Bills.ElementAt( 1 ).CreationDateAsString, "03.01.2015" );
            Assert.AreEqual( _pm.Bills.ElementAt( 1 ).DocumentText, "CourtOfJustice" );
        }
    }
}

