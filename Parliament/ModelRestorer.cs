﻿/********************************************************************************************/
using System.Collections.Generic;
using Parliament.Repository;
using Parliament.Repository.EntityFramework;
/********************************************************************************************/


namespace Parliament
{
    class ModelRestorer
    {
        private ParliamentDbContext m_dbContext;

/*------------------------------------------------------------------------------------------*/

        public ModelRestorer ( ParliamentDbContext _dbContext )
        {
            m_dbContext = _dbContext;
        }

/*------------------------------------------------------------------------------------------*/

        public ParliamentModel Restore ()
        {
            ParliamentModel PM = new ParliamentModel();

            RestoreCollection( RepositoryFactory.MakeAdministratorRepository( m_dbContext ), PM.Administrators );
            RestoreCollection( RepositoryFactory.MakeUserRepository( m_dbContext ), PM.Users );
            RestoreCollection( RepositoryFactory.MakeSessionRepository( m_dbContext ), PM.Sessions );
                               
            RestoreCollection( RepositoryFactory.MakeRDocumentCreatorRepository( m_dbContext ), PM.RDocumentCreators );
            RestoreCollection( RepositoryFactory.MakeUDocumentCreatorRepository( m_dbContext ), PM.UDocumentCreators );
                               
            RestoreCollection( RepositoryFactory.MakeRevisionRepository( m_dbContext ), PM.Revisions );
            RestoreCollection( RepositoryFactory.MakeBillRepository( m_dbContext ), PM.Bills );
            RestoreCollection( RepositoryFactory.MakeLawRepository( m_dbContext ), PM.Laws );

            return PM;
        }

/*------------------------------------------------------------------------------------------*/

        private void RestoreCollection < TEntity > (
                IRepository < TEntity > repository
            ,   ICollection < TEntity > target
        )   where TEntity : Utils.Entity
        {
            foreach ( TEntity obj in repository.LoadAll() )
                target.Add( obj );
        }
    }
}


/********************************************************************************************/