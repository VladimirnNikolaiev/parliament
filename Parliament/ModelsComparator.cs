﻿/********************************************************************************************/
using System;
using System.IO;
using System.Text;
/********************************************************************************************/


namespace Parliament
{
    static class ModelsComparator
    {

        public static void CompareModels ( ParliamentModel model1, ParliamentModel model2 )
        {
            StringWriter writer1 = new StringWriter( new StringBuilder() );
            var reportGenerator1 = new ModelReporter( writer1 );
            reportGenerator1.GenerateReports( model1 );

            StringWriter writer2 = new StringWriter( new StringBuilder() );
            var reportGenerator2 = new ModelReporter( writer2) ;
            reportGenerator2.GenerateReports( model2 );

            String report1Content = writer1.ToString();
            String report2Content = writer2.ToString();

            Console.WriteLine( report1Content );

            Console.WriteLine( "================================================================" );
            Console.WriteLine();

            Console.WriteLine( report2Content );

            Console.WriteLine( "================================================================" );
            Console.WriteLine();

            Console.WriteLine( report1Content.Equals( report2Content ) 
                ?   "PASSED: Models match" 
                :   "FAILED: Models mismatch"
            );
        }
    }
}


/********************************************************************************************/