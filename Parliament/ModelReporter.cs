﻿/********************************************************************************************/
using System.IO;
using System.Collections.Generic;
using Parliament.Model;
/********************************************************************************************/


namespace Parliament
{
    class ModelReporter
    {
        TextWriter m_output;

/*------------------------------------------------------------------------------------------*/

        public ModelReporter ( TextWriter _output )
        {
            m_output = _output;
        }

/*------------------------------------------------------------------------------------------*/

        private void ReportCollection< T > ( string _title, ICollection<T> _items )
        {
            m_output.WriteLine( "==== {0} ==== ", _title );
            m_output.WriteLine();

            foreach ( var item in _items )
            {
                m_output.WriteLine( item );
                m_output.WriteLine();
            }

            m_output.WriteLine();
        }

/*------------------------------------------------------------------------------------------*/

        private void PrintRegisteredUnregistered ( string _title, ICollection< DocumentCreator > _items )
        {
            m_output.WriteLine( "==== {0} ==== ", _title );
            m_output.WriteLine();

            foreach ( var item in _items )
            {
                m_output.WriteLine( item.getName() );
                m_output.WriteLine();
            }

            m_output.WriteLine();
        }

/*------------------------------------------------------------------------------------------*/

        public void GenerateReports( ParliamentModel _pm )
        {
            ReportCollection( "Administrators", _pm.Administrators );
            ReportCollection( "Users", _pm.Users );
            ReportCollection( "Sessions", _pm.Sessions );

            ReportCollection( "Bills", _pm.Bills );
            ReportCollection( "Laws", _pm.Laws );
            ReportCollection( "Registered document creators", _pm.RDocumentCreators );
            ReportCollection( "Unregistered document creators", _pm.UDocumentCreators );
        }
    }
}


/********************************************************************************************/