﻿/********************************************************************************************/
using System.Collections.Generic;
using Parliament.Model;
/********************************************************************************************/


namespace Parliament
{
    class ParliamentModel
    {
        public ICollection < Administrator > Administrators { get; private set; }
        public ICollection < User > Users { get; private set; }
        public ICollection < Session > Sessions { get; private set; }
                             
        public ICollection < Bill > Bills { get; private set; }
        public ICollection < Law > Laws { get; private set; }
        public ICollection < Revision > Revisions { get; private set; }
        public ICollection < RegisteredDocumentCreator > RDocumentCreators { get; private set; }
        public ICollection < UnregisteredDocumentCreator > UDocumentCreators { get; private set; }


        public ParliamentModel ()
        {
            Administrators  = new List< Administrator >();
            Users           = new List< User >();
            Sessions        = new List< Session >();

            Bills           = new List< Bill >();
            Laws            = new List< Law >();
            Revisions       = new List< Revision >();

            RDocumentCreators = new List< RegisteredDocumentCreator >();
            UDocumentCreators = new List< UnregisteredDocumentCreator >();
        }
    }
}


/********************************************************************************************/