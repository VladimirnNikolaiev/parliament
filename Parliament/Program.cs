﻿/********************************************************************************************/
using System;
using Microsoft.Practices.Unity;
using Parliament.Repository.EntityFramework;
using Parliament.Dependencies;
/********************************************************************************************/


namespace Parliament
{
    class Program
    {
        static void Main( string[] args )
        {            
            try
            {
                using ( var dbContext = new ParliamentDbContext() )
                using ( var unityContainer = new UnityContainer() )
                {
                    dbContext.Database.Initialize( true );

                    ContainerBootstraper.RegisterTypes( unityContainer, dbContext );

                    TestServiceGenerator generator = new TestServiceGenerator( unityContainer );

                    generator.GenerateTestData();
                }

                using ( var dbContext = new ParliamentDbContext() )
                using ( var unityContainer = new UnityContainer() )
                {
                    ContainerBootstraper.RegisterTypes( unityContainer, dbContext );
                
                    ServiceReporter reportGenerator = new ServiceReporter( unityContainer, Console.Out );
                    reportGenerator.GenerateReports();
                }
            }

            catch ( Exception e )
            {
                while (e != null)
                {
                    Console.WriteLine("{0}: {1}", e.GetType().FullName, e.Message);

                    e = e.InnerException;
                }
            }
        }

/*------------------------------------------------------------------------------------------*/

        private static ParliamentModel generateModelAndRunTest () 
        {
            ParliamentModel model = new ParliamentModel();
            TestModelGenerator tm = new TestModelGenerator();

            tm.GenerateTestData( model );

            return model;
        }

/*------------------------------------------------------------------------------------------*/

        private static void testOrm ( ParliamentModel _sourceModel )
        {
            using ( var dbContext = new ParliamentDbContext() )
            {
                ModelSaver saver = new ModelSaver( dbContext );
                saver.Save( _sourceModel );
            }

            using ( var dbContext = new ParliamentDbContext() )
            {
                ModelRestorer restorer = new ModelRestorer( dbContext );
                ParliamentModel restoredModel = restorer.Restore();

                ModelsComparator.CompareModels( _sourceModel, restoredModel );
            }
        }
    }
}


/********************************************************************************************/
