﻿/********************************************************************************************/
using Parliament.Repository;
using Parliament.Repository.EntityFramework;
using System.Collections.Generic;
/********************************************************************************************/


namespace Parliament
{
    class ModelSaver
    {
        private ParliamentDbContext m_dbContext;

 /*------------------------------------------------------------------------------------------*/

        public ModelSaver ( ParliamentDbContext _dbContext )
        {
            m_dbContext = _dbContext;
        }

/*------------------------------------------------------------------------------------------*/

        public void Save( ParliamentModel model )
        {
            SaveCollection( RepositoryFactory.MakeAdministratorRepository( m_dbContext ), model.Administrators );
            SaveCollection( RepositoryFactory.MakeUserRepository( m_dbContext ), model.Users );
            SaveCollection( RepositoryFactory.MakeSessionRepository( m_dbContext ), model.Sessions );
                            
            SaveCollection( RepositoryFactory.MakeRDocumentCreatorRepository( m_dbContext ), model.RDocumentCreators );
            SaveCollection( RepositoryFactory.MakeUDocumentCreatorRepository( m_dbContext ), model.UDocumentCreators );
                            
            SaveCollection( RepositoryFactory.MakeRevisionRepository( m_dbContext ), model.Revisions );
            SaveCollection( RepositoryFactory.MakeBillRepository( m_dbContext ), model.Bills );
            SaveCollection( RepositoryFactory.MakeLawRepository( m_dbContext ), model.Laws );
        }

/*------------------------------------------------------------------------------------------*/

        private void SaveCollection < TRepository, TEntity > ( 
                TRepository repository
            ,   ICollection<TEntity> collection
        )   where TRepository : IRepository < TEntity >
            where TEntity : Utils.Entity
        {
            foreach ( TEntity obj in collection )
                repository.Add( obj );

            repository.Commit();
        }
    }
}


/********************************************************************************************/