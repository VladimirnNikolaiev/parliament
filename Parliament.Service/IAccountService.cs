﻿/********************************************************************************************/
using System;
using Parliament.Dto;

using Parliament.Utils.Validators;
using Parliament.Service.Validators;
/********************************************************************************************/


namespace Parliament.Service
{
    public interface IAccountService<_DtoType> : IDomainEntityService
        where _DtoType : AccountDto
    {
        _DtoType Identify (
                [ EmailValidator ] string _email
            ,   [ NonEmptyStringValidator ] string _password
        );

        _DtoType View ( Guid _domainId );

        void ChangeName ( Guid _accountId, [ NonEmptyStringValidator ] string _newName );

        void ChangeEmail ( Guid _accountId, [ EmailValidator ] string _newEmail );

        void ChangePassword (
                Guid _accountId
            ,   [ NonEmptyStringValidator ] string _oldPassword
            ,   [ NonEmptyStringValidator ] string _newPassword 
        );

        void editAccountStatus ( 
                Guid _accountId
            ,   [ AccountStatusValidator ] string _newStatus 
        );
    }
}


/********************************************************************************************/