﻿/********************************************************************************************/
using System;
using System.Collections.Generic;
using Parliament.Dto;

using Parliament.Utils.Validators;
using Parliament.Service.Validators;
/********************************************************************************************/


namespace Parliament.Service
{
    public interface IUserService : IAccountService< UserDto >
    {
        IList< LawDto > ViewAssociatedLaws ( Guid _accountId );

        IList< BillDto > ViewAssociatedBills ( Guid _accountId );

        Guid createNew();

        Guid createNew (
                [ EmailValidator ] string _email
            ,   [ NonEmptyStringValidator ] string _password
            ,   [ NonEmptyStringValidator ] string _name
            ,   [ PostValidator ] string _userPost
            ,   [ NonEmptyStringValidator ] string _imageUrl
        );
    }
}


/********************************************************************************************/