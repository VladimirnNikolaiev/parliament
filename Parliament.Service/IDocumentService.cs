﻿/********************************************************************************************/
using System;
using System.Collections.Generic;
using Parliament.Dto;

using Parliament.Utils.Validators;
using Parliament.Service.Validators;
/********************************************************************************************/


namespace Parliament.Service
{
    public interface IDocumentService< _DtoType > : IDomainEntityService where _DtoType : DomainEntityDto
    {
        _DtoType ViewFullInformation ( Guid _documentId );

        IList< RevisionDto > ViewHistoryOfRevisions ( Guid _documentId );

        string ViewStatus ( Guid _documentId);

        void addRevision ( 
                Guid _documentId
            ,   [ NonEmptyStringValidator ] string _documentFilePath
            ,   [ CreationDateTimeValidator ] DateTime _creationDate
            ,   IList< Guid > _documentCreators
        );
    }
}


/********************************************************************************************/