﻿/********************************************************************************************/
using System;

using Microsoft.Practices.EnterpriseLibrary.Validation;
using Microsoft.Practices.EnterpriseLibrary.Validation.Validators;

using Parliament.Model;
/********************************************************************************************/

namespace Parliament.Service.Validators
{

    [AttributeUsage( AttributeTargets.Property |
              AttributeTargets.Field |
              AttributeTargets.Parameter )
    ]
    class AccessRightsValidator : ValidatorAttribute
    {
        protected override Validator DoCreateValidator ( Type targetType )
        {
            return new EnumConversionValidator( typeof( Administrator.AdministratorAccessRight ) );
        }
    }
}
