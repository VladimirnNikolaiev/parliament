﻿/********************************************************************************************/
using Parliament.Dto;
using Parliament.Service.Validators;
using Parliament.Utils.Validators;
using System;
using System.Collections.Generic;
/********************************************************************************************/


namespace Parliament.Service
{
    public interface IBillService : IDocumentService< BillDto >
    {
        Guid createNew ();

        Guid addNew (
                [ NonEmptyStringValidator ] string _name
            ,   [ BillStatusValidator ] string _status
            ,   [ NonEmptyStringValidator ] string _documentFilePath
            ,   [ CreationDateTimeValidator ] DateTime _creationDate
            ,   IList< Guid > _documentCreator
        );

        void editStatus (
                Guid _documentId
            ,   [ BillStatusValidator ] string _newStatus
        );

    }
}


/********************************************************************************************/