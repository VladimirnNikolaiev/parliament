﻿/********************************************************************************************/
using Parliament.Dto;
using System;

using Parliament.Utils.Validators;
using Parliament.Service.Validators;
/********************************************************************************************/

namespace Parliament.Service
{
    public interface IUnregisteredDocumentCreatorService 
        : IDocumentCreatorService< UnregisteredDocumentCreatorDto >
    {
        Guid addNew ( 
            [ NonEmptyStringValidator ]string _name,
            [ RoleValidator ]string _role
            );
    }
}


/********************************************************************************************/