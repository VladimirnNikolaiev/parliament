﻿/********************************************************************************************/
using Parliament.Dto;
using System;

using Parliament.Service.Validators;
using Parliament.Utils.Validators;
using System.Collections.Generic;
/********************************************************************************************/


namespace Parliament.Service
{
    public interface ILawService : IDocumentService< LawDto >
    {
        Guid createNew ();

        void editStatus (
                Guid _documentId
            ,   [ LawStatusValidator ] string _newStatus
        );

        Guid addNew ( 
                [ NonEmptyStringValidator ] string _name
            ,   [ LawStatusValidator ] string _status
            ,   [ NonEmptyStringValidator ] string _documentFilePath
            ,   [ CreationDateTimeValidator ] DateTime _creationDate
            ,   IList< Guid > _documentCreator
        );
    }
}


/********************************************************************************************/