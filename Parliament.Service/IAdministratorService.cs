﻿/********************************************************************************************/
using System;
using Parliament.Dto;

using Parliament.Utils.Validators;
using Parliament.Service.Validators;
/********************************************************************************************/


namespace Parliament.Service
{
    public interface IAdministratorService : IAccountService< AdministratorDto >
    {
        Guid createNew ();

        Guid createNew ( 
                [ EmailValidator ] string _email
            ,   [ NonEmptyStringValidator ] string _password
            ,   [ NonEmptyStringValidator ] string _name
            ,   [ AccessRightsValidator ] string _accessRight 
            ,   [ NonEmptyStringValidator ] string _imageUrl
        );
    }
}


/********************************************************************************************/