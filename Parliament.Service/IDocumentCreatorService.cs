﻿/********************************************************************************************/
using System;
/********************************************************************************************/


namespace Parliament.Service
{
    public interface IDocumentCreatorService< _DtoType > : IDomainEntityService
    {
        _DtoType View ( Guid _domainId );

        void addBillToCreator ( Guid _creatorId, Guid _billId );
        void addLawToCreator ( Guid _creatorId, Guid _lawId );
    }
}


/********************************************************************************************/