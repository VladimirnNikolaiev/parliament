﻿/********************************************************************************************/
using Parliament.Dto;
using System;
/********************************************************************************************/

namespace Parliament.Service
{
    public interface IRegisteredDocumentCreatorService 
        : IDocumentCreatorService< RegisteredDocumentCreatorDto >
    {
        Guid addNew ( Guid _userId );
    }
}


/********************************************************************************************/