﻿/********************************************************************************************/
using Parliament.Model;
/********************************************************************************************/


namespace Parliament.Repository.EntityFramework.Repository_Implementations
{
    public class SessionRepository : BasicRepository< Session >, ISessionRepository
    {
        public SessionRepository ( ParliamentDbContext _dbContext )
            :   base ( _dbContext, _dbContext.Sessions )
        {
            // Yet nothing to do.
        }
    }
}


/********************************************************************************************/