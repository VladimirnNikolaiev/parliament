﻿/********************************************************************************************/
using System;
using System.Linq;
using Parliament.Model;
/********************************************************************************************/


namespace Parliament.Repository.EntityFramework.Repository_Implementations
{
    public class AdministratorRepository : BasicRepository< Administrator >, IAdministratorRepository
    {
        public AdministratorRepository ( ParliamentDbContext _dbContext )
            :   base ( _dbContext, _dbContext.Administrators )
        {
        }

/*------------------------------------------------------------------------------------------*/

        public Administrator FindByName ( string _name )
        {
            return GetDBSet().Where( a => a.Name == _name ).SingleOrDefault();
        }

/*------------------------------------------------------------------------------------------*/

        public Administrator FindById ( Guid _administratorId )
        {
             return GetDBSet().Where( a => a.DomainId == _administratorId ).SingleOrDefault();
        }

/*------------------------------------------------------------------------------------------*/

        public Administrator FindByEmail ( string _email )
        {
            return GetDBSet().Where( u => u.Email == _email ).SingleOrDefault();
        }

/*------------------------------------------------------------------------------------------*/

        public IQueryable< Guid > SelectDocumentManagementAdminsByIds ()
        {
            return GetDBSet()
                .Where( a => a.AccessRight == Administrator.AdministratorAccessRight.DocumentManagement )
                .Select( a => a.DomainId ); 
        }

/*------------------------------------------------------------------------------------------*/

        public IQueryable< Guid > SelectAccountsManagementAdminsByIds ()
        {
            return GetDBSet()
                .Where( a => a.AccessRight == Administrator.AdministratorAccessRight.AccountsManagement )
                .Select( a => a.DomainId );
        }

/*------------------------------------------------------------------------------------------*/

        public IQueryable< Guid > SelectFullRigthsAdminsByIds ()
        {
            return GetDBSet()
                .Where( a => a.AccessRight == Administrator.AdministratorAccessRight.Full )
                .Select( a => a.DomainId );
        }
    }
}


/********************************************************************************************/