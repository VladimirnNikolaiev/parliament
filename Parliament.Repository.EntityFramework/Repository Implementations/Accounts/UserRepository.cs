﻿/********************************************************************************************/
using System;
using System.Linq;
using Parliament.Model;
/********************************************************************************************/


namespace Parliament.Repository.EntityFramework.Repository_Implementations
{
    public class UserRepository : BasicRepository< User >, IUserRepository
    {
        public UserRepository ( ParliamentDbContext _dbContext )
            :   base ( _dbContext, _dbContext.Users )
        {
        }

/*------------------------------------------------------------------------------------------*/

        public User FindByName ( string _name )
        {
            return GetDBSet().Where( u => u.Name == _name ).SingleOrDefault();
        }

/*------------------------------------------------------------------------------------------*/

        public User FindById ( Guid _userId )
        {
            return GetDBSet().Where( u => u.DomainId == _userId).SingleOrDefault();
        }

 /*------------------------------------------------------------------------------------------*/

        public User FindByEmail ( string _email )
        {
            return GetDBSet().Where( u => u.Email == _email ).SingleOrDefault();
        }

/*------------------------------------------------------------------------------------------*/

        public IQueryable< Guid > SelectDepytiesByIds ()
        {
            return GetDBSet()
                .Where( d => d.Post == User.UserPost.Deputy )
                .Select( d => d.DomainId );
        }

/*------------------------------------------------------------------------------------------*/

        public IQueryable< Guid > SelectSpeakersByIds ()
        {
            return GetDBSet()
                .Where( s => s.Post == User.UserPost.Speaker )
                .Select( s => s.DomainId );
        }
    }
}


/********************************************************************************************/