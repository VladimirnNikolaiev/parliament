﻿/********************************************************************************************/
using System;
using System.Linq;
using System.Data.Entity;
/********************************************************************************************/


namespace Parliament.Repository.EntityFramework.Repository_Implementations
{
    public abstract class BasicRepository< T > where T : Parliament.Utils.Entity
    {
        private ParliamentDbContext m_dbContext;
        private DbSet< T > m_dbSet;

/*------------------------------------------------------------------------------------------*/

        protected BasicRepository ( ParliamentDbContext _dbContext, DbSet< T > _dbSet )
        {
            m_dbContext = _dbContext;
            m_dbSet = _dbSet;
        }

/*------------------------------------------------------------------------------------------*/

        protected ParliamentDbContext GetDBContext ()
        {
            return m_dbContext;
        }

/*------------------------------------------------------------------------------------------*/

        protected DbSet< T > GetDBSet ()
        {
            return m_dbSet;
        }

/*------------------------------------------------------------------------------------------*/

        public void Add ( T _obj )
        {
            m_dbSet.Add( _obj );
        }

/*------------------------------------------------------------------------------------------*/

        public void Delete ( T _obj )
        {
            m_dbSet.Remove( _obj );
        }

/*------------------------------------------------------------------------------------------*/

        virtual public IQueryable< T > LoadAll ()
        {
            return m_dbSet;
        }

/*------------------------------------------------------------------------------------------*/

        virtual public T Load ( int _id )
        {
            return m_dbSet.Find(_id);
        }

/*------------------------------------------------------------------------------------------*/

        public int Count ()
        {
            return m_dbSet.Count();
        }

/*------------------------------------------------------------------------------------------*/

        virtual public T FindByDomainId ( Guid domainId )
        {
            return m_dbSet.Where( e => e.DomainId == domainId ).SingleOrDefault();
        }

/*------------------------------------------------------------------------------------------*/

        public IQueryable< Guid > SelectAllDomainIds ()
        {
            return m_dbSet.Select( e => e.DomainId );
        }

 /*------------------------------------------------------------------------------------------*/

        public void StartTransaction ()
        {
            m_dbContext.Database.BeginTransaction();
        }

/*------------------------------------------------------------------------------------------*/

        public void Commit ()
        {
            m_dbContext.ChangeTracker.DetectChanges();
            m_dbContext.SaveChanges();

            m_dbContext.Database.CurrentTransaction.Commit();
        }

/*------------------------------------------------------------------------------------------*/

        public void Rollback ()
        {
            m_dbContext.Database.CurrentTransaction.Rollback();
        }
    }
}


/********************************************************************************************/