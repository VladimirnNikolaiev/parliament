﻿/********************************************************************************************/
using System;
using System.Linq;
using Parliament.Model;
/********************************************************************************************/


namespace Parliament.Repository.EntityFramework.Repository_Implementations
{
    public class UnregisteredDocumentCreatorRepository 
        : BasicDocumentCreatorRepository< UnregisteredDocumentCreator >, IUnregisteredDocumentCreatorRepository
    {
        public UnregisteredDocumentCreatorRepository ( ParliamentDbContext _dbContext )
            :   base ( _dbContext, _dbContext.UnregisteredDocumentCreators )
        {
        }

/*------------------------------------------------------------------------------------------*/

        public UnregisteredDocumentCreator FindByName ( string _name )
        {
            return GetDBSet().Where( uDC => uDC.Name == _name ).SingleOrDefault();
        }

/*------------------------------------------------------------------------------------------*/

        public UnregisteredDocumentCreator FindUnregisteredDCById ( Guid _unregisteredDCId )
        {
            return GetDBSet().Where( uDC => uDC.DomainId == _unregisteredDCId ).SingleOrDefault();
        }

/*------------------------------------------------------------------------------------------*/

        public IQueryable< Guid > SelectPresidentsAsUnregisteredDC ()
        {
            return GetDBSet()
                .Where( uDC => uDC.Role == UnregisteredDocumentCreator.UnregDCRole.President )
                .Select( b => b.DomainId );
        }

/*------------------------------------------------------------------------------------------*/

        public IQueryable< Guid > SelectPrimeMinistersAsUnregisteredDC ()
        {
            return GetDBSet()
                .Where( uDC => uDC.Role == UnregisteredDocumentCreator.UnregDCRole.PrimeMinister )
                .Select( b => b.DomainId );
        }

/*------------------------------------------------------------------------------------------*/

        public IQueryable< Guid > SelectVicePrimeMinistersAsUnregisteredDC ()
        {
            return GetDBSet()
                .Where( uDC => uDC.Role == UnregisteredDocumentCreator.UnregDCRole.VicePrimeMinister )
                .Select( b => b.DomainId );
        }

/*------------------------------------------------------------------------------------------*/

        public IQueryable< Guid > SelectMinistersOfInternalAffairsAsUnregisteredDC ()
        {
            return GetDBSet()
                .Where( uDC => uDC.Role == UnregisteredDocumentCreator.UnregDCRole.MinisterOfInternalAffairs )
                .Select( b => b.DomainId );
        }

/*------------------------------------------------------------------------------------------*/

        public IQueryable< Guid > SelectMinistersOfJusticeAsUnregisteredDC ()
        {
            return GetDBSet()
                .Where( uDC => uDC.Role == UnregisteredDocumentCreator.UnregDCRole.MinisterOfJustice )
                .Select( b => b.DomainId );
        }

/*------------------------------------------------------------------------------------------*/

        public IQueryable< Guid > SelectMinistersOfFinancesAsUnregisteredDC ()
        {
            return GetDBSet()
                .Where( uDC => uDC.Role == UnregisteredDocumentCreator.UnregDCRole.MinisterOfFinances )
                .Select( b => b.DomainId );
        }

/*------------------------------------------------------------------------------------------*/

        public IQueryable< Guid > SelectMinistersOfEducationAsUnregisteredDC ()
        {
            return GetDBSet()
                .Where( uDC => uDC.Role == UnregisteredDocumentCreator.UnregDCRole.MinisterOfEducation )
                .Select( b => b.DomainId );
        }

/*------------------------------------------------------------------------------------------*/

        public IQueryable< Guid > SelectMinistersOfHealthAsUnregisteredDC ()
        {
            return GetDBSet()
                .Where( uDC => uDC.Role == UnregisteredDocumentCreator.UnregDCRole.MinisterOfHealth )
                .Select( b => b.DomainId );
        }

/*------------------------------------------------------------------------------------------*/

        public IQueryable< Guid > SelectMinistersOfSocialPolicyAsUnregisteredDC ()
        {
            return GetDBSet()
                .Where( uDC => uDC.Role == UnregisteredDocumentCreator.UnregDCRole.MinisterOfSocialPolicy )
                .Select( b => b.DomainId );
        }

/*------------------------------------------------------------------------------------------*/

        public IQueryable< Guid > SelectMinisterSOfEcologyAsUnregisteredDC ()
        {
            return GetDBSet()
                .Where( uDC => uDC.Role == UnregisteredDocumentCreator.UnregDCRole.MinisterOfEcology )
                .Select( b => b.DomainId );
        }

/*------------------------------------------------------------------------------------------*/

        public IQueryable< Guid > SelectMinistersOfCultureAsUnregisteredDC ()
        {
            return GetDBSet()
                .Where( uDC => uDC.Role == UnregisteredDocumentCreator.UnregDCRole.MinisterOfCulture )
                .Select( b => b.DomainId );
        }

/*------------------------------------------------------------------------------------------*/

        public IQueryable< Guid > SelectMinistersOfForeignAffairsAsUnregisteredDC ()
        {
            return GetDBSet()
                .Where( uDC => uDC.Role == UnregisteredDocumentCreator.UnregDCRole.MinisterOfForeignAffairs )
                .Select( b => b.DomainId );
        }

/*------------------------------------------------------------------------------------------*/

        public IQueryable< Guid > SelectMinisterOfAgrarianPolicyAsUnregisteredDC ()
        {
            return GetDBSet()
                .Where( uDC => uDC.Role == UnregisteredDocumentCreator.UnregDCRole.MinisterOfAgrarianPolicy )
                .Select( b => b.DomainId );
        }

/*------------------------------------------------------------------------------------------*/

        public IQueryable< Guid > SelectMinistersOfFuelAndEnergyAsUnregisteredDC ()
        {
            return GetDBSet()
                .Where( uDC => uDC.Role == UnregisteredDocumentCreator.UnregDCRole.MinisterOfFuelAndEnergy )
                .Select( b => b.DomainId );
        }

/*------------------------------------------------------------------------------------------*/

        public IQueryable< Guid > SelectMinistersOfYouthAndSportsAsUnregisteredDC ()
        {
            return GetDBSet()
                .Where( uDC => uDC.Role == UnregisteredDocumentCreator.UnregDCRole.MinisterOfYouthAndSports )
                .Select( b => b.DomainId );
        }

/*------------------------------------------------------------------------------------------*/

        public IQueryable< Guid > SelectMinistersOfInrastructureAsUnregisteredDC ()
        {
            return GetDBSet()
                .Where( uDC => uDC.Role == UnregisteredDocumentCreator.UnregDCRole.MinisterOfInrastructure )
                .Select( b => b.DomainId );
        }

/*------------------------------------------------------------------------------------------*/

        public IQueryable< Guid > SelectMinistersOfDefenceAsUnregisteredDC ()
        {
            return GetDBSet()
                .Where( uDC => uDC.Role == UnregisteredDocumentCreator.UnregDCRole.MinisterOfDefence )
                .Select( b => b.DomainId );
        }

/*------------------------------------------------------------------------------------------*/

        public IQueryable< Guid > SelectPublicFiguresAsUnregisteredDC ()
        {
            return GetDBSet()
                .Where( uDC => uDC.Role == UnregisteredDocumentCreator.UnregDCRole.PublicFigure )
                .Select( b => b.DomainId );
        }
    }
}


/********************************************************************************************/