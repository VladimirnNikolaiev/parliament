﻿/********************************************************************************************/
using System;
using System.Linq;
using Parliament.Model;
/********************************************************************************************/


namespace Parliament.Repository.EntityFramework.Repository_Implementations
{
    public class RegisteredDocumentCreatorRepository 
        : BasicDocumentCreatorRepository< RegisteredDocumentCreator >, IRegisteredDocumentCreatorRepository
    {
        public RegisteredDocumentCreatorRepository ( ParliamentDbContext _dbContext )
            :   base ( _dbContext, _dbContext.RegisteredDocumentCreators )
        {
        }

/*------------------------------------------------------------------------------------------*/

        public RegisteredDocumentCreator FindByName ( string _name )
        {
            return GetDBSet().Where( rDC => rDC.getName() == _name ).SingleOrDefault();
        }

/*------------------------------------------------------------------------------------------*/

        public RegisteredDocumentCreator FindRegisteredDCById ( Guid _registeredDCId )
        {
            return GetDBSet().Where( rDC => rDC.DomainId == _registeredDCId ).SingleOrDefault();
        }

/*------------------------------------------------------------------------------------------*/

        public RegisteredDocumentCreator FindRegisteredDcByUser ( User _user )
        {
            return GetDBSet().Where( rDc => rDc.Account == _user ).SingleOrDefault();
        }
    }
}


/********************************************************************************************/