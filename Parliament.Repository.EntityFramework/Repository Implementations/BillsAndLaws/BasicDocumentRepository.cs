﻿/********************************************************************************************/
using System.Data.Entity;
using Parliament.Model;
using System.Linq;
using System;
/********************************************************************************************/

namespace Parliament.Repository.EntityFramework.Repository_Implementations
{
    public abstract class BasicDocumentRepository<T> : BasicRepository<T> where T : Document
    {
        private DbSet<RegisteredDocumentCreator> m_rDcSet;
        private DbSet<UnregisteredDocumentCreator> m_urDcSet;
        private DbSet<Revision> m_revisionsSet;

/*------------------------------------------------------------------------------------------*/

        public BasicDocumentRepository ( ParliamentDbContext _dbContext, DbSet<T> _dbSet ) 
            :   base( _dbContext, _dbSet )
        {
            m_rDcSet = _dbContext.RegisteredDocumentCreators;
            m_urDcSet = _dbContext.UnregisteredDocumentCreators;
            m_revisionsSet = _dbContext.Revisions;
        }

/*------------------------------------------------------------------------------------------*/

        public override T Load ( int _id )
        {
            loadDependencies();
            return base.Load( _id );
        }

/*------------------------------------------------------------------------------------------*/

        public override IQueryable< T > LoadAll ()
        {
            loadDependencies();
            return base.LoadAll();
        }

/*------------------------------------------------------------------------------------------*/

        public override T FindByDomainId ( Guid domainId )
        {
            loadDependencies();
            return base.FindByDomainId( domainId );
        }

/*------------------------------------------------------------------------------------------*/

        private void loadDependencies ()
        {
            m_rDcSet.ToList();
            m_urDcSet.ToList();
            m_revisionsSet.ToList();
        }
    }
}
