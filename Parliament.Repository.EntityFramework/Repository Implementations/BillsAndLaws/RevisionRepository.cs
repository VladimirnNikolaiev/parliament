﻿/********************************************************************************************/
using System;
using System.Linq;
using Parliament.Model;
/********************************************************************************************/


namespace Parliament.Repository.EntityFramework.Repository_Implementations
{
    public class RevisionRepository : BasicRepository< Revision >, IRevisionRepository
    {
        public RevisionRepository ( ParliamentDbContext _dbContext )
            : base ( _dbContext, _dbContext.Revisions )
        {
        }

/*------------------------------------------------------------------------------------------*/

        public  Revision FindRevisionById ( Guid _revisionId )
        {
            return GetDBSet().Where( r => r.DomainId == _revisionId ).SingleOrDefault();
        }

/*------------------------------------------------------------------------------------------*/

        public Revision FindRevisionByCreationDate ( DateTime _creationDate )
        {
            return GetDBSet().Where( r => r.CreationDate == _creationDate ).SingleOrDefault();
        }

/*------------------------------------------------------------------------------------------*/

        public Revision FindRevisionByMajorCreator ( DocumentCreator _majorCreator )
        {
            return GetDBSet().Where( r => r.MajorCreator == _majorCreator ).SingleOrDefault();
        }
    }
}


/********************************************************************************************/
