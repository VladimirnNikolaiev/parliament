﻿/********************************************************************************************/
using System.Data.Entity;
using Parliament.Model;
using System.Linq;
using System;
/********************************************************************************************/

namespace Parliament.Repository.EntityFramework.Repository_Implementations
{
    public abstract class BasicDocumentCreatorRepository<T> : BasicRepository<T> where T : DocumentCreator
    {
        private DbSet<Bill> m_billsSet;
        private DbSet<Law> m_lawsSet;

/*------------------------------------------------------------------------------------------*/

        public BasicDocumentCreatorRepository ( ParliamentDbContext _dbContext, DbSet<T> _dbSet ) 
            : base( _dbContext, _dbSet )
        {
            m_billsSet = _dbContext.Bills;
            m_lawsSet = _dbContext.Laws;

            m_billsSet.ToList();
            m_lawsSet.ToList();
        }

/*------------------------------------------------------------------------------------------*/

        public override T Load ( int _id )
        {
            loadDependencies();
            return base.Load( _id );
        }

/*------------------------------------------------------------------------------------------*/

        public override IQueryable<T> LoadAll ()
        {
            loadDependencies();
            return base.LoadAll();
        }

/*------------------------------------------------------------------------------------------*/

        public override T FindByDomainId ( Guid domainId )
        {
            loadDependencies();
            return base.FindByDomainId( domainId );
        }

/*------------------------------------------------------------------------------------------*/

        private void loadDependencies ()
        {
            m_billsSet.ToList();
            m_lawsSet.ToList();
        }
    }
}
