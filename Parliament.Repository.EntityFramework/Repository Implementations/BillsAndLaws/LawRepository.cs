﻿/********************************************************************************************/
using System;
using System.Linq;
using Parliament.Model;
/********************************************************************************************/


namespace Parliament.Repository.EntityFramework.Repository_Implementations
{
    public class LawRepository : BasicDocumentRepository< Law >, ILawRepository
    {
        public LawRepository ( ParliamentDbContext _dbContext )
            :   base ( _dbContext, _dbContext.Laws )
        {
        }

 /*------------------------------------------------------------------------------------------*/

        public Law FindByName ( string _name )
        {
            return GetDBSet().Where( l => l.Name == _name ).SingleOrDefault();
        }

/*------------------------------------------------------------------------------------------*/

        public Law FindLawById ( Guid _lawId )
        {
            return GetDBSet().Where( l => l.DomainId == _lawId ).SingleOrDefault();
        }

/*------------------------------------------------------------------------------------------*/

        public IQueryable< Guid > SelectActLawsByIds ()
        {
            return GetDBSet()
                .Where( l => l.Status == Law.LawsStatus.Act )
                .Select( l => l.DomainId );
        }

/*------------------------------------------------------------------------------------------*/

        public IQueryable< Guid > SelectRemovedLawsByIds ()
        {
            return GetDBSet()
                .Where( l => l.Status == Law.LawsStatus.Removed )
                .Select( l => l.DomainId );
        }
    }
}


/********************************************************************************************/