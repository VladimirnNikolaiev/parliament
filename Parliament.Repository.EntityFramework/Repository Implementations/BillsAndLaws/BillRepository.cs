﻿/********************************************************************************************/
using System;
using System.Linq;
using Parliament.Model;
/********************************************************************************************/


namespace Parliament.Repository.EntityFramework.Repository_Implementations
{
    public class BillRepository : BasicDocumentRepository< Bill >, IBillRepository
    {
        public BillRepository ( ParliamentDbContext _dbContext )
            :   base ( _dbContext, _dbContext.Bills )
        {
        }

 /*------------------------------------------------------------------------------------------*/

        public Bill FindByName ( string _name )
        {
            return GetDBSet().Where( b => b.Name == _name ).SingleOrDefault();
        }

/*------------------------------------------------------------------------------------------*/

        public Bill FindBillById ( Guid _billId )
        {
            return GetDBSet().Where( b => b.DomainId == _billId ).SingleOrDefault();
        }

/*------------------------------------------------------------------------------------------*/

        public IQueryable< Guid > SelectNewBillsByIds ()
        {
             return GetDBSet()
                .Where( b => b.Status == Bill.BillsStatus.New )
                .Select( b => b.DomainId );
        }

/*------------------------------------------------------------------------------------------*/

        public IQueryable< Guid > SelectComesIntoForceBillsByIds ()
        {
            return GetDBSet()
                .Where( b => b.Status == Bill.BillsStatus.ComesIntoForce )
                .Select( b => b.DomainId );
        }

/*------------------------------------------------------------------------------------------*/

        public IQueryable< Guid > SelectAwaitsPresidentsSignatureBillsByIds ()
        {
            return GetDBSet()
                .Where( b => b.Status == Bill.BillsStatus.AwaitsPresidentsSignature )
                .Select( b => b.DomainId );
        }

/*------------------------------------------------------------------------------------------*/

        public IQueryable< Guid > SelectVetoedBillsByIds ()
        {
            return GetDBSet()
                .Where( b => b.Status == Bill.BillsStatus.Vetoed )
                .Select( b => b.DomainId );
        }

/*------------------------------------------------------------------------------------------*/

        public IQueryable< Guid > SelectVotedAsAWholeBillsByIds ()
        {
            return GetDBSet()
                .Where( b => b.Status == Bill.BillsStatus.VotedAsAWhole )
                .Select( b => b.DomainId );
        }

/*------------------------------------------------------------------------------------------*/

        public IQueryable< Guid > SelectVotedIn1stReadingBillsByIds ()
        {
            return GetDBSet()
                .Where( b => b.Status == Bill.BillsStatus.VotedIn1stReading )
                .Select( b => b.DomainId );
        }

/*------------------------------------------------------------------------------------------*/

        public IQueryable< Guid > SelectVotedIn2stReadingBillsByIds ()
        {
            return GetDBSet()
                .Where( b => b.Status == Bill.BillsStatus.VotedIn2ndReading )
                .Select( b => b.DomainId );
        }

/*------------------------------------------------------------------------------------------*/

        public IQueryable< Guid > SelectVotedIn3stReadingBillsByIds ()
        {
            return GetDBSet()
                .Where( b => b.Status == Bill.BillsStatus.VotedIn3rdReading )
                .Select( b => b.DomainId );
        }

/*------------------------------------------------------------------------------------------*/

        public IQueryable< Guid > SelectIncludedToAgendaBillsByIds ()
        {
            return GetDBSet()
                .Where( b => b.Status == Bill.BillsStatus.IncludedToAgenda )
                .Select( b => b.DomainId );
        }
    }
}


/********************************************************************************************/