﻿/********************************************************************************************/
using Parliament.Repository.EntityFramework.Repository_Implementations;
/********************************************************************************************/


namespace Parliament.Repository.EntityFramework
{
    public static class RepositoryFactory
    {
        public static IAdministratorRepository MakeAdministratorRepository (
            ParliamentDbContext _dbcontext 
        )
        {
            return new AdministratorRepository( _dbcontext );
        }

/*------------------------------------------------------------------------------------------*/

        public static IUserRepository MakeUserRepository ( ParliamentDbContext _dbcontext )
        {
            return new UserRepository( _dbcontext );
        }

/*------------------------------------------------------------------------------------------*/

        public static ISessionRepository MakeSessionRepository ( ParliamentDbContext _dbcontext )
        {
            return new SessionRepository( _dbcontext );
        }

/*------------------------------------------------------------------------------------------*/

        public static IBillRepository MakeBillRepository ( ParliamentDbContext _dbcontext )
        {
            return new BillRepository( _dbcontext );
        }

/*------------------------------------------------------------------------------------------*/

        public static ILawRepository MakeLawRepository ( ParliamentDbContext _dbcontext)
        {
            return new LawRepository( _dbcontext );
        }

/*------------------------------------------------------------------------------------------*/

        public static IRevisionRepository MakeRevisionRepository ( ParliamentDbContext _dbcontext )
        {
            return new RevisionRepository( _dbcontext );
        }

/*------------------------------------------------------------------------------------------*/

        public static IRegisteredDocumentCreatorRepository 
        MakeRDocumentCreatorRepository ( ParliamentDbContext _dbcontext )
        {
            return new RegisteredDocumentCreatorRepository( _dbcontext );
        }

/*------------------------------------------------------------------------------------------*/

        public static IUnregisteredDocumentCreatorRepository
        MakeUDocumentCreatorRepository ( ParliamentDbContext _dbcontext )
        {
            return new UnregisteredDocumentCreatorRepository( _dbcontext );
        }        
    }
}


/********************************************************************************************/