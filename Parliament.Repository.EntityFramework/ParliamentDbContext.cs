﻿/********************************************************************************************/
using Parliament.Model;
using System.Data.Entity;
/********************************************************************************************/


namespace Parliament.Repository.EntityFramework
{
    public class ParliamentDbContext : DbContext
    {
        static ParliamentDbContext ()
        {
            Database.SetInitializer (
                new DropCreateDatabaseAlways< ParliamentDbContext >()
            );
        }

/*------------------------------------------------------------------------------------------*/

        public ParliamentDbContext ()
        {
            Database.Log = ( s => System.Diagnostics.Debug.WriteLine( s ) );
        }

/*------------------------------------------------------------------------------------------*/

        public DbSet< Administrator > Administrators    { get; set; }
        public DbSet< User > Users                      { get; set; }
        public DbSet< Session > Sessions                { get; set; }
                      
        public DbSet< Bill > Bills                      { get; set; }
        public DbSet< Law > Laws                        { get; set; }
        public DbSet< Revision > Revisions              { get; set; }

        public DbSet< RegisteredDocumentCreator > RegisteredDocumentCreators        { get; set; }
        public DbSet< UnregisteredDocumentCreator > UnregisteredDocumentCreators    { get; set; }

/*------------------------------------------------------------------------------------------*/

        protected override void OnModelCreating ( DbModelBuilder modelBuilder )
        {
            modelBuilder.Configurations.Add( new Configurations.AccountConfiguration< Account >() );
            modelBuilder.Configurations.Add( new Configurations.AdministratorConfiguration() );
            modelBuilder.Configurations.Add( new Configurations.UserConfiguration() );
            modelBuilder.Configurations.Add( new Configurations.SessionConfiguration() );
                                             
            modelBuilder.Configurations.Add( new Configurations.DocumentConfiguration< Document >() );
            modelBuilder.Configurations.Add( new Configurations.BillConfiguration() );
            modelBuilder.Configurations.Add( new Configurations.LawConfiguration() );
            modelBuilder.Configurations.Add( new Configurations.RevisionConfiguration() );
                                             
            modelBuilder.Configurations.Add( new Configurations.DocumentTextConfiguration() );
            modelBuilder.Configurations.Add( new Configurations.DocumentInfoConfiguration() );
                                             
            modelBuilder.Configurations.Add( new Configurations.DocumentCreatorConfiguration< DocumentCreator>() );
            modelBuilder.Configurations.Add( new Configurations.RegisteredDocumentCreatorConfiguration() );
            modelBuilder.Configurations.Add( new Configurations.UnregisteredDocumentCreatorConfiguration() );
        }
    }
}


/********************************************************************************************/