﻿/********************************************************************************************/
using Parliament.Model;
/********************************************************************************************/


namespace Parliament.Repository.EntityFramework.Configurations
{
    class AccountConfiguration< T > : BasicEntityConfiguration< T >
        where T : Account
    {
        public AccountConfiguration ()
        {
            Property( a => a.Name ).IsRequired();
            Property( a => a.Password ).IsRequired();
            Property( a => a.Email ).IsRequired();
            Property( a => a.Status ).IsRequired();
        }
    }
}


/********************************************************************************************/