﻿/********************************************************************************************/
using Parliament.Model;
/********************************************************************************************/


namespace Parliament.Repository.EntityFramework.Configurations
{
    class DocumentCreatorConfiguration< T > : BasicEntityConfiguration< T >
        where T : DocumentCreator
    {
        public DocumentCreatorConfiguration ()
        {
            HasMany< Document >( dc => dc.Documents );
        }
    }
}


/********************************************************************************************/