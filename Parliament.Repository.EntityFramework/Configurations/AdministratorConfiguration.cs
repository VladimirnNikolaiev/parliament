﻿/********************************************************************************************/
using Parliament.Model;
/********************************************************************************************/


namespace Parliament.Repository.EntityFramework.Configurations
{
    class AdministratorConfiguration : AccountConfiguration< Administrator >
    {
        public AdministratorConfiguration ()
        {
            Property( a => a.AccessRight ).IsRequired();
        }
    }
}


/********************************************************************************************/