﻿/********************************************************************************************/
using System.Data.Entity.ModelConfiguration;
/********************************************************************************************/


namespace Parliament.Repository.EntityFramework.Configurations
{
    abstract class BasicValueConfiguration< TValue > : ComplexTypeConfiguration< TValue >
         where TValue : class
    {
        protected BasicValueConfiguration ()
        {
        }
    }
}


/********************************************************************************************/