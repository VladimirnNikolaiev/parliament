﻿/********************************************************************************************/
using Parliament.Model;
/********************************************************************************************/


namespace Parliament.Repository.EntityFramework.Configurations
{
    class LawConfiguration : DocumentConfiguration< Law >
    {
        public LawConfiguration ()
        {
            Property( l => l.Status ).IsRequired();
        }
    }
}


/********************************************************************************************/