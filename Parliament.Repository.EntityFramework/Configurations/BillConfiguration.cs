﻿/********************************************************************************************/
using Parliament.Model;
/********************************************************************************************/


namespace Parliament.Repository.EntityFramework.Configurations
{
    class BillConfiguration : DocumentConfiguration< Bill >
    {
        public BillConfiguration ()
        {
            Property( b => b.Status ).IsRequired();
        }
    }
}


/********************************************************************************************/