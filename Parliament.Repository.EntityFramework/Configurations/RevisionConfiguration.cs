﻿/********************************************************************************************/
using Parliament.Model;
/********************************************************************************************/


namespace Parliament.Repository.EntityFramework.Configurations
{
    class RevisionConfiguration : BasicEntityConfiguration< Revision >
    {
        public RevisionConfiguration()
        {
            Property( r => r.CreationDate ).IsRequired();

            HasMany< DocumentCreator >( r => r.Creators );
        }
    }
}


/********************************************************************************************/