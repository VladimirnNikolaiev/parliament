﻿/********************************************************************************************/
using Parliament.Model;
/********************************************************************************************/


namespace Parliament.Repository.EntityFramework.Configurations
{
    class RegisteredDocumentCreatorConfiguration : DocumentCreatorConfiguration< RegisteredDocumentCreator >
    {
        public RegisteredDocumentCreatorConfiguration ()
        { 
            HasRequired( rdc => rdc.Account );      // Поменял в RegisteredDocumentCreator свойство Account
        }                                           // с protected на public. Set сделал private.
    }
}


/********************************************************************************************/