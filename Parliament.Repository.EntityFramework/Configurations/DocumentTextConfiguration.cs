﻿/********************************************************************************************/
using Parliament.Model;
/********************************************************************************************/


namespace Parliament.Repository.EntityFramework.Configurations
{
    class DocumentTextConfiguration : BasicValueConfiguration< DocumentText >
    {
        public DocumentTextConfiguration ()
        {
            Property( dt => dt.DocumentPath ).IsRequired();
        }
    }
}


/********************************************************************************************/