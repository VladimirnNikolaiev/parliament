﻿/********************************************************************************************/
using Parliament.Model;
/********************************************************************************************/


namespace Parliament.Repository.EntityFramework.Configurations
{
    class DocumentConfiguration< T > : BasicEntityConfiguration< T >
        where T: Document
    {
        public DocumentConfiguration ()
        {
            Property( b => b.Name ).IsRequired();

            HasMany< Revision > ( b => b.Revisions );
        }
    }
}


/********************************************************************************************/