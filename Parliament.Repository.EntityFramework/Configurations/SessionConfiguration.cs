﻿/********************************************************************************************/
using Parliament.Model;
/********************************************************************************************/


namespace Parliament.Repository.EntityFramework.Configurations
{
    class SessionConfiguration : BasicEntityConfiguration< Session >
    {
        public SessionConfiguration()
        {
            Property( s => s.OpenTime ).IsRequired();
            Ignore( s => s.CloseTime ); 
            Ignore( s => s.Status );

            HasRequired( s => s.Account );
        }
    }
}


/********************************************************************************************/