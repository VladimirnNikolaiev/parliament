﻿/********************************************************************************************/
using Parliament.Model;
/********************************************************************************************/


namespace Parliament.Repository.EntityFramework.Configurations
{
    class UserConfiguration : AccountConfiguration< User >
    {
        public UserConfiguration ()
        {
            Property( a => a.Post ).IsRequired();
        }
    }
}


/********************************************************************************************/