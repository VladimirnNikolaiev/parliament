﻿/********************************************************************************************/
using System.Data.Entity.ModelConfiguration;
/********************************************************************************************/


namespace Parliament.Repository.EntityFramework.Configurations
{
    abstract class BasicEntityConfiguration< T > : EntityTypeConfiguration< T >
        where T : Parliament.Utils.Entity
    {
        protected BasicEntityConfiguration ()
        {
            HasKey( e => e.DatabaseId );
            Property( e => e.DomainId ).IsRequired();
        }
    }
}


/********************************************************************************************/