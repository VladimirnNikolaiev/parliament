﻿/********************************************************************************************/
using Parliament.Model;
/********************************************************************************************/


namespace Parliament.Repository.EntityFramework.Configurations
{
    class UnregisteredDocumentCreatorConfiguration : DocumentCreatorConfiguration< UnregisteredDocumentCreator >
    {
        public UnregisteredDocumentCreatorConfiguration ()
        {
            Property( urdc => urdc.Role ).IsRequired();
            Property( urdc => urdc.Name ).IsRequired();
        }
    }
}


/********************************************************************************************/