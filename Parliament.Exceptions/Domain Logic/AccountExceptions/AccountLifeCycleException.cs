﻿/********************************************************************************************/
using System;
/********************************************************************************************/


namespace Parliament.Exceptions
{
    public class AccountLifeCycleException : DomainLogicException
    {
        private AccountLifeCycleException ( string _message )
            : base ( _message )
        {}

/*------------------------------------------------------------------------------------------*/

        public static AccountLifeCycleException AccountActs ( Guid _accountId )
        {
            return new AccountLifeCycleException( 
                string.Format( 
                        "Account #{0} may not be added as it already acts."
                    ,   _accountId
                )
            );
        }

/*------------------------------------------------------------------------------------------*/

        public static AccountLifeCycleException AccountConcludedPowers ( Guid _accountId )
        {
            return new AccountLifeCycleException( 
                string.Format( 
                        "Account #{0} can not be reached or created as it's power concluded."
                    ,   _accountId
                )
            );
        }
    }
}


/********************************************************************************************/