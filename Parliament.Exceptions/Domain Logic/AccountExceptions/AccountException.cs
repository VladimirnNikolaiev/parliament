﻿/********************************************************************************************/
using System;
/********************************************************************************************/


namespace Parliament.Exceptions
{
    public class AccountException : DomainLogicException
    {
        private AccountException( string _message )
            : base( _message )
        {}

/*------------------------------------------------------------------------------------------*/

        public static AccountException AccessRight( Guid _id, string _accountType )
        {
            return new AccountException(
                string.Format( 
                        "{0} account #{1} has not got required access right." 
                    ,   _accountType
                    ,   _id
                )
            );
        }

/*------------------------------------------------------------------------------------------*/

        public static AccountException FailPassword ( Guid _id, string _accountType )
        {
            return new AccountException(
                string.Format( 
                        "{0} account #{1} password check failed." 
                    ,   _accountType
                    ,   _id
                )
            );
        }
    }
}


/********************************************************************************************/