﻿/********************************************************************************************/
using System;
/********************************************************************************************/


namespace Parliament.Exceptions
{
    public class DocumentException : DomainLogicException
    {
        private DocumentException ( string _message )
            : base( _message ) 
        {}

/*------------------------------------------------------------------------------------------*/

        public static DocumentException InvalidDocumentStatus ( 
                string _documentType /*Bill or Law*/ 
            ,   Guid _documentId
        )
        {
            return new DocumentException(
                string.Format(
                        "{0}'s #{1} status is not allowed. "
                    +   "Are you trying to create a new Bill or Law that was vetoed or removed?"
                    ,   _documentType
                    ,   _documentId
                )    
            );
        }

/*------------------------------------------------------------------------------------------*/

        public static DocumentException EmptyDocumentRevisions (
                Guid _documentId
            ,   string _documentType /*Bill or Law*/
        )
        {
             return new DocumentException(
                string.Format(
                        "{0} #{1} amount of revisions is empty."
                    ,   _documentType
                    ,   _documentId
                )    
            );
        }

/*------------------------------------------------------------------------------------------*/

        public static DocumentException SameDocument ( Guid _documentId )
        {
             return new DocumentException(
                string.Format(
                        "Document #{0} already created by this User."
                    ,   _documentId
                )    
            );
        }
    }
}


/********************************************************************************************/