﻿/********************************************************************************************/
using System;
/********************************************************************************************/


namespace Parliament.Exceptions
{
    public class RevisionException : DomainLogicException
    {
        private RevisionException ( string _message )
            : base( _message )
        {}

/*------------------------------------------------------------------------------------------*/

        public static RevisionException InvalidCreationDate ( Guid _revisionId )
        {
            return new RevisionException(
                string.Format(
                        "Creation date of revision #{0} is not allowed."
                    ,   _revisionId
                )    
            );
        }

/*------------------------------------------------------------------------------------------*/

        public static RevisionException EmptyRevisionInfo ( Guid _revisionId )
        {
            return new RevisionException(
                string.Format(
                        "Revision info of revision #{0} is empty."
                    ,   _revisionId
                )    
            );
        }

/*------------------------------------------------------------------------------------------*/

        public static RevisionException EmptyDocumentCreators ( Guid _revisionId )
        {
            return new RevisionException(
                string.Format(
                        "Amount of creators of revision #{0} is empty."
                    ,   _revisionId
                )    
            );
        }

/*------------------------------------------------------------------------------------------*/

        public static RevisionException SameRevision ( Guid _revisionId )
        {
            return new RevisionException(
                string.Format(
                        "Revision #{0} you are trying to add already exists for current document."
                    ,   _revisionId
                )    
            );
        }

/*------------------------------------------------------------------------------------------*/

        public static RevisionException SameDocumentCreator ( Guid _documentCreatorId )
        {
            return new RevisionException(
                string.Format(
                        "Document Creator #{0} you are trying to add already exists for current document."
                    ,   _documentCreatorId
                )    
            );
        }
    }
}


/********************************************************************************************/