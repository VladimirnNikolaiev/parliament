﻿/********************************************************************************************/
using System;
/********************************************************************************************/


namespace Parliament.Exceptions
{
    public class DocumentCreatorException : DomainLogicException
    {
        private DocumentCreatorException ( string _message )
            : base( _message )
        {}

/*------------------------------------------------------------------------------------------*/

        public static DocumentCreatorException InvalidCreatedDocument ( 
                Guid _documentId 
            ,   string _documentType
        )
        {
            return new DocumentCreatorException(
                string.Format(
                        "The {0} #{1} does not exist in list of created documents "
                    +   "for current document creator."
                    ,   _documentType
                    ,   _documentId
                )
            );
        }

/*------------------------------------------------------------------------------------------*/

        public static DocumentCreatorException UnregisteredDocumentCreator ( Guid _documentCreatorId )
        {
            return new DocumentCreatorException(
                string.Format(
                        "User #{0} is not Registered Document Creator."
                    ,   _documentCreatorId
                )
            );
        }

/*------------------------------------------------------------------------------------------*/

        public static DocumentCreatorException UnregisteredDocumentCreatorType ( 
            Guid _unregisteredDocumentCreator 
        )
        {
             return new DocumentCreatorException(
                string.Format(
                        "Unregistered Document Creator #{0} has not gog required Role."
                    ,   _unregisteredDocumentCreator
                )
            );
        }
    }
}


/********************************************************************************************/