﻿/********************************************************************************************/
using System;
/********************************************************************************************/


namespace Parliament.Exceptions
{
    public class DomainLogicException : Exception
    {
         public DomainLogicException ( string _message )
            :   base( _message )
        {}
    }
}


/********************************************************************************************/