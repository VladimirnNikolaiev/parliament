﻿/********************************************************************************************/
using System;
/********************************************************************************************/


namespace Parliament.Exceptions
{
    public class ServiceValidationException : Exception
    {
        public ServiceValidationException ( string message )
            : base( message )
        {}
    }
}


/********************************************************************************************/