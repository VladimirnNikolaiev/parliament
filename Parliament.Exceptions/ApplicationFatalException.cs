﻿/********************************************************************************************/
using System;
/********************************************************************************************/


namespace Parliament.Exceptions
{
    public class ApplicationFatalException : Exception
    {
        public ApplicationFatalException ( string message )
            : base( message )
        {}
    }
}


/********************************************************************************************/