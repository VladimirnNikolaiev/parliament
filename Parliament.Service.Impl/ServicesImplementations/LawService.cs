﻿/********************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;

using Parliament.Dto;
using Parliament.Repository;
using Parliament.Model;
using Parliament.Exceptions;

using Microsoft.Practices.Unity;
/********************************************************************************************/


namespace Parliament.Service.Impl
{
    public class LawService : DocumentService, ILawService
    {
        [ Dependency ]
        protected ILawRepository lawRepository { set; get; }

/*------------------------------------------------------------------------------------------*/

        public Guid createNew ()
        {
            Law law = new Law( Guid.NewGuid(), "", Law.LawsStatus.None, null );
            lawRepository.Add( law );

            return law.DomainId;
        }

/*------------------------------------------------------------------------------------------*/

        public Guid addNew (
                string _name
            ,   string _status
            ,   string _documentFilePath
            ,   DateTime _creationDate
            ,   IList< Guid > _documentCreator
        )
        {
            Revision revision = createNewRevision( _documentFilePath, _creationDate, _documentCreator );
            commitRevision( revision );

            Law newLaw = new Law (
                    Guid.NewGuid()
                ,   _name
                ,   ( Law.LawsStatus )Enum.Parse( ( typeof( Law.LawsStatus ) ), _status )
                ,   revision
            );

            lawRepository.Add( newLaw );

            return newLaw.DomainId;
        }

/*------------------------------------------------------------------------------------------*/

        public void addRevision ( 
                Guid _documentId
            ,   string _documentFilePath
            ,   DateTime _creationDate
            ,   IList< Guid > _documentCreators 
        )
        {
            check( _documentId );

            Revision newRevision = createNewRevision( _documentFilePath, _creationDate, _documentCreators );

            commitRevision( newRevision );

            Law law = resolveLaw( _documentId );
            law.addRevision( newRevision );
        }

/*------------------------------------------------------------------------------------------*/

        public void editStatus ( Guid _documentId, string _newStatus )
        {
            if ( _newStatus == Law.LawsStatus.Removed.ToString() )
                throw DocumentException.InvalidDocumentStatus( "Law", _documentId );

            Law law = resolveLaw( _documentId );
            law.Status = ( Law.LawsStatus )Enum.Parse( typeof( Law.LawsStatus ), _newStatus );
        }

/*------------------------------------------------------------------------------------------*/

        public IList< Guid > ViewAll ()
        {
            return lawRepository.SelectAllDomainIds().ToList();
        }

/*------------------------------------------------------------------------------------------*/

        public LawDto ViewFullInformation ( Guid _documentId )
        {
            return resolveLaw( _documentId ).toDto();
        }

/*------------------------------------------------------------------------------------------*/

        public IList< RevisionDto > ViewHistoryOfRevisions( Guid _documentId )
        {
            List< RevisionDto > revisions = new List< RevisionDto >();

            Law law = resolveLaw( _documentId );
            foreach ( Revision revision in law.Revisions )
                revisions.Add( revision.toDto() );

            return revisions;
        }

/*------------------------------------------------------------------------------------------*/

        public string ViewStatus ( Guid _documentId )
        {
            return resolveLaw ( _documentId ).Status.ToString();
        }

/*------------------------------------------------------------------------------------------*/

        private Law resolveLaw ( Guid _documentId )
        {
            return ServiceUtils.ResolveEntity( lawRepository, _documentId );
        }

/*------------------------------------------------------------------------------------------*/

        private void check( Guid _lawId )
        {
            Law law = lawRepository.FindByDomainId( _lawId );
            if( law == null )
                throw new ServiceUnresolvedEntityException( typeof( Bill ), _lawId );
        }
    }
}


/********************************************************************************************/