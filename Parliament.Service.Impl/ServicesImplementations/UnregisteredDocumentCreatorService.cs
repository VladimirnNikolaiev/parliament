﻿/********************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;

using Parliament.Dto;
using Parliament.Repository;
using Parliament.Model;

using Microsoft.Practices.Unity;
/********************************************************************************************/


namespace Parliament.Service.Impl
{
    public class UnregisteredDocumentCreatorService : IUnregisteredDocumentCreatorService
    {
        [ Dependency ]
        protected IUnregisteredDocumentCreatorRepository creatorRepository { set; get; }

        [ Dependency ]
        protected IUserRepository userRepository { set; get; }

        [ Dependency ]
        protected IBillRepository billRepository { set; get; }

        [ Dependency ]
        protected ILawRepository lawRepository { set; get; }

/*------------------------------------------------------------------------------------------*/

        public void addBillToCreator ( Guid _creatorId, Guid _billId )
        {
            Bill bill = resolveBill( _billId );
            UnregisteredDocumentCreator creator = resolveDocumentCreator( _creatorId );

            creator.addCreatedDocument( bill );
        }

/*------------------------------------------------------------------------------------------*/

        public void addLawToCreator ( Guid _creatorId, Guid _lawId )
        {
            Law law = resolveLaw( _lawId );
            UnregisteredDocumentCreator creator = resolveDocumentCreator( _creatorId );

            creator.addCreatedDocument( law );
        }

/*------------------------------------------------------------------------------------------*/

        public Guid addNew ( string _name, string _role )
        {
            UnregisteredDocumentCreator creator = new UnregisteredDocumentCreator (
                    Guid.NewGuid()
                ,   _name
                ,   ( UnregisteredDocumentCreator.UnregDCRole )Enum.Parse (
                            typeof( UnregisteredDocumentCreator.UnregDCRole )
                        ,   _role
                    )
            );

            creatorRepository.Add( creator );

            return creator.DomainId;
        }

/*------------------------------------------------------------------------------------------*/

        public UnregisteredDocumentCreatorDto View ( Guid _domainId )
        {
            return resolveDocumentCreator( _domainId ).toDto();
        }

/*------------------------------------------------------------------------------------------*/

        public IList< Guid > ViewAll ()
        {
            return creatorRepository.SelectAllDomainIds().ToList();
        }

/*------------------------------------------------------------------------------------------*/

        private UnregisteredDocumentCreator resolveDocumentCreator ( Guid _domainId )
        {
            return ServiceUtils.ResolveEntity( creatorRepository, _domainId );
        }

/*------------------------------------------------------------------------------------------*/

        private User resolveUser( Guid _domainId )
        {
            return ServiceUtils.ResolveEntity( userRepository, _domainId );
        }

/*------------------------------------------------------------------------------------------*/

        private Law resolveLaw( Guid _domainId )
        {
            return ServiceUtils.ResolveEntity( lawRepository, _domainId );
        }

/*------------------------------------------------------------------------------------------*/

        private Bill resolveBill( Guid _domainId )
        {
            return ServiceUtils.ResolveEntity( billRepository, _domainId );
        }
    }
}


/********************************************************************************************/