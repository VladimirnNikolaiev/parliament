﻿/********************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;

using Parliament.Dto;
using Parliament.Repository;
using Parliament.Model;

using Microsoft.Practices.Unity;
/********************************************************************************************/


namespace Parliament.Service.Impl
{
    public class RegisteredDocumentCreatorService : IRegisteredDocumentCreatorService
    {
        [ Dependency ]
        protected IRegisteredDocumentCreatorRepository creatorRepository { set; get; }

        [ Dependency ]
        protected IUserRepository userRepository { set; get; }

        [ Dependency ]
        protected IBillRepository billRepository { set; get; }

        [ Dependency ]
        protected ILawRepository lawRepository { set; get; }

/*------------------------------------------------------------------------------------------*/

        public void addBillToCreator ( Guid _creatorId, Guid _billId )
        {
            Bill bill = resolveBill( _billId );

            RegisteredDocumentCreator creator = resolveDocumentCreator( _creatorId );

            creator.addCreatedDocument( bill );
        }

/*------------------------------------------------------------------------------------------*/

        public void addLawToCreator ( Guid _creatorId, Guid _lawId )
        {
            Law law = resolveLaw( _lawId );
            RegisteredDocumentCreator creator = resolveDocumentCreator( _creatorId );

            creator.addCreatedDocument( law );
        }

/*------------------------------------------------------------------------------------------*/

        public Guid addNew ( Guid _userId )
        {
            User user = resolveUser( _userId );

            RegisteredDocumentCreator creator = new RegisteredDocumentCreator(
                    Guid.NewGuid()
                ,   user
            );

            creatorRepository.Add( creator );

            return creator.DomainId;
        }

/*------------------------------------------------------------------------------------------*/

        public RegisteredDocumentCreatorDto View ( Guid _domainId )
        {
            return resolveDocumentCreator( _domainId ).toDto();
        }

/*------------------------------------------------------------------------------------------*/

        public IList< Guid > ViewAll()
        {
            return creatorRepository.SelectAllDomainIds().ToList();
        }

/*------------------------------------------------------------------------------------------*/

        private RegisteredDocumentCreator resolveDocumentCreator( Guid _domainId )
        {
            return ServiceUtils.ResolveEntity( creatorRepository, _domainId );
        }

/*------------------------------------------------------------------------------------------*/

        private User resolveUser( Guid _domainId )
        {
            return ServiceUtils.ResolveEntity( userRepository, _domainId );
        }

/*------------------------------------------------------------------------------------------*/

        private Law resolveLaw( Guid _domainId )
        {
            return ServiceUtils.ResolveEntity( lawRepository, _domainId );
        }

/*------------------------------------------------------------------------------------------*/

        private Bill resolveBill( Guid _domainId )
        {
            return ServiceUtils.ResolveEntity( billRepository, _domainId );
        }
    }
}


/********************************************************************************************/