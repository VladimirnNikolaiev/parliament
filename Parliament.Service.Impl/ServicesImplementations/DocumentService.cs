﻿/********************************************************************************************/
using System;
using System.Collections.Generic;

using Parliament.Model;
using Parliament.Repository;
using Parliament.Exceptions;

using Microsoft.Practices.Unity;
/********************************************************************************************/


namespace Parliament.Service.Impl
{
    public abstract class DocumentService
    {
        [ Dependency ]
        protected IRevisionRepository revisionRepository { set; get; }

        [ Dependency ]
        protected IRegisteredDocumentCreatorRepository registeredDcRepository { set; get; }

        [ Dependency ]
        protected IUnregisteredDocumentCreatorRepository unregisteredDcRepository { set; get; }

/*------------------------------------------------------------------------------------------*/

        protected Revision createNewRevision ( 
                string _documentFilePath
            ,   DateTime _creationDate
            ,   IList< Guid > _documentCreators
        )
        {
            var revision = new Revision (
                    Guid.NewGuid()
                ,   _creationDate
                ,   new DocumentInfo( _documentFilePath )
            );

            var creators = collectCreators( _documentCreators );

            foreach ( DocumentCreator creator in creators )
                revision.addCreator( creator );

            return revision;
        }

/*------------------------------------------------------------------------------------------*/

        protected IList< DocumentCreator > collectCreators( IList< Guid > _documentCreators )
        {
            List< DocumentCreator > creators = new List< DocumentCreator >();

            foreach ( var creatorId in _documentCreators )
            {
                DocumentCreator creator;

                creator = unregisteredDcRepository.FindByDomainId( creatorId );
                if ( creator == null )
                    creator = registeredDcRepository.FindByDomainId( creatorId );

                if ( creator == null )
                    throw new ServiceUnresolvedEntityException( typeof( Bill ), creatorId );

                var udc = unregisteredDcRepository.FindByDomainId( creatorId );

                if ( udc != null )
                    creators.Add( udc );
                else
                    creators.Add( registeredDcRepository.FindByDomainId( creatorId ) );
            }

            return creators;
        }

/*------------------------------------------------------------------------------------------*/

        protected void commitRevision( Revision _revision )
        {
            revisionRepository.Add( _revision );
        }
    }
}


/********************************************************************************************/