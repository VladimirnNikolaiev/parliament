﻿/********************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;

using Parliament.Dto;
using Parliament.Repository;
using Parliament.Model;
using Parliament.Exceptions;

using Microsoft.Practices.Unity;
/********************************************************************************************/


namespace Parliament.Service.Impl
{
    public class BillService : DocumentService, IBillService
    {
        [ Dependency ]
        protected IBillRepository billRepository { set; get; }

/*------------------------------------------------------------------------------------------*/

        public Guid createNew ()
        {
            Bill bill = new Bill( Guid.NewGuid(), "", Bill.BillsStatus.None, null );
            billRepository.Add( bill );

            return bill.DomainId;
        }

/*------------------------------------------------------------------------------------------*/

        public Guid addNew ( 
                string _name
            ,   string _status
            ,   string _documentFilePath
            ,   DateTime _creationDate
            ,   IList< Guid > _documentCreator
        )
        {
            Revision revision = createNewRevision( _documentFilePath, _creationDate, _documentCreator );
            commitRevision( revision );

            Bill newBill = new Bill (
                    Guid.NewGuid()
                ,   _name
                ,   ( Bill.BillsStatus )Enum.Parse (
                            ( typeof( Bill.BillsStatus ) )
                        ,   _status
                    )
                ,   revision
            );

            billRepository.Add( newBill );
            return newBill.DomainId;
        }

/*------------------------------------------------------------------------------------------*/

        public void addRevision ( 
                Guid _documentId
            ,   string _documentFilePath
            ,   DateTime _creationDate
            ,   IList< Guid > _documentCreators 
        )
        {
            check( _documentId );

            Revision newRevision = createNewRevision( _documentFilePath, _creationDate, _documentCreators );

            commitRevision( newRevision );

            Bill bill = resolveBill( _documentId );
            bill.addRevision( newRevision );
        }

/*------------------------------------------------------------------------------------------*/

        public void editStatus ( Guid _documentId, string _newStatus )
        {
            check( _documentId );

            if ( _newStatus == Bill.BillsStatus.Vetoed.ToString() )
                throw DocumentException.InvalidDocumentStatus( "Bill", _documentId ); 

            Bill bill = resolveBill( _documentId );
            bill.Status = ( Bill.BillsStatus )Enum.Parse( typeof( Bill.BillsStatus ), _newStatus );
        }

/*------------------------------------------------------------------------------------------*/

        public IList< Guid > ViewAll ()
        {
            return billRepository.SelectAllDomainIds().ToList();
        }

/*------------------------------------------------------------------------------------------*/

        public BillDto ViewFullInformation ( Guid _documentId )
        {
            return resolveBill( _documentId ).toDto();
        }

/*------------------------------------------------------------------------------------------*/

        public IList< RevisionDto > ViewHistoryOfRevisions ( Guid _documentId )
        {
            List< RevisionDto > revisions = new List< RevisionDto >();

            Bill bill = resolveBill( _documentId );
            foreach ( Revision revision in bill.Revisions )
                revisions.Add( revision.toDto() );

            return revisions;
        }

/*------------------------------------------------------------------------------------------*/

        public string ViewStatus ( Guid _documentId )
        {
            return resolveBill( _documentId ).Status.ToString();
        }

/*------------------------------------------------------------------------------------------*/

        private Bill resolveBill ( Guid _billId )
        {
            return ServiceUtils.ResolveEntity( billRepository, _billId );
        }

/*------------------------------------------------------------------------------------------*/

        private void check( Guid _billId )
        {
            Bill bill = billRepository.FindByDomainId( _billId );
            if( bill == null )
                throw new ServiceUnresolvedEntityException( typeof( Bill ), _billId );
        }
    }
}


/********************************************************************************************/