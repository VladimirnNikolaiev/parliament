﻿/********************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;

using Parliament.Dto;
using Parliament.Repository;
using Parliament.Model;
using Parliament.Exceptions;

using Microsoft.Practices.Unity;
/********************************************************************************************/


namespace Parliament.Service.Impl
{
    public class AdministratorService : IAdministratorService
    {

        [ Dependency ]
        protected IAdministratorRepository administratorRepository { get; set; }

/*------------------------------------------------------------------------------------------*/

        public IList< Guid > ViewAll ()
        {
            return administratorRepository.SelectAllDomainIds().ToList();
        }

/*------------------------------------------------------------------------------------------*/

        public AdministratorDto Identify ( string _email, string _password )
        {
            Administrator a = administratorRepository.FindByEmail( _email );
            if ( a == null )
                return null;

            if ( !a.checkPassword( _password ) )
                throw AccountException.FailPassword( a.DomainId, "Administrator" );


            return a.toDto();
        }

/*------------------------------------------------------------------------------------------*/

        public AdministratorDto View ( Guid _domainId )
        {
            return ResolveAdministrator( _domainId ).toDto();
        }

/*------------------------------------------------------------------------------------------*/

        public void ChangeName ( Guid _accountId, string _newName )
        {
            Administrator administrator = ResolveAdministrator( _accountId );
            administrator.Name = _newName;
        }

/*------------------------------------------------------------------------------------------*/

        public void ChangeEmail( Guid _accountId, string _newEmail )
        {
            Administrator administrator = ResolveAdministrator( _accountId );

            if( administrator.Email == _newEmail )
                throw new DuplicateNamedEntityException( typeof( Administrator ), _newEmail );

            administrator.Email = _newEmail;
        }

/*------------------------------------------------------------------------------------------*/

        public void ChangePassword ( Guid _accountId, string _oldPassword, string _newPassword )
        {
            Administrator administrator = ResolveAdministrator( _accountId );

            if ( !administrator.checkPassword( _oldPassword ) )
                throw AccountException.FailPassword( _accountId, "Administrator" );

            administrator.Password = _newPassword;
        }

/*------------------------------------------------------------------------------------------*/

        public void editAccountStatus ( Guid _accountId, string _newStatus )
        {
            Administrator admin = ResolveAdministrator( _accountId );

            if( _newStatus == Account.AccountStatus.ConcludedPowers.ToString() )
                throw AccountLifeCycleException.AccountConcludedPowers( _accountId );

            admin.Status = ( Account.AccountStatus )Enum.Parse (
                    typeof( Account.AccountStatus )
                ,   _newStatus
            );
        }

/*------------------------------------------------------------------------------------------*/

        public Guid createNew ()
        {
            // NOTE: Will be established by use of AdministratorService.
            Administrator newAdministrator = new Administrator (
                    Guid.NewGuid()
                ,   "" /* _name */
                ,   "" /* _password */
                ,   "" /* _email */
                ,   User.AccountStatus.None
                ,   Administrator.AdministratorAccessRight.None
                ,   "" /* _imageUrl */
            );

            administratorRepository.Add( newAdministrator );

            return newAdministrator.DomainId;
        }

/*------------------------------------------------------------------------------------------*/

        public Guid createNew ( string _email, string _password, string _name, string _accessRight, string _imageUrl )
        {
            Administrator administrator = administratorRepository.FindByEmail( _email );
            if ( administrator != null )
                throw new DuplicateNamedEntityException( typeof( Administrator ), _email );

            Administrator newAdministrator = new Administrator (
                    Guid.NewGuid()
                ,   _name
                ,   _password
                ,   _email
                ,   User.AccountStatus.Act
                ,   ( Administrator.AdministratorAccessRight )Enum.Parse (
                            typeof( Administrator.AdministratorAccessRight )
                        ,   _accessRight
                    )
                ,   _imageUrl
            );

            administratorRepository.Add( newAdministrator );

            return newAdministrator.DomainId;
        }

/*------------------------------------------------------------------------------------------*/

        private Administrator ResolveAdministrator ( Guid administratorId )
        {
            return ServiceUtils.ResolveEntity( administratorRepository, administratorId );
        }
    }
}


/********************************************************************************************/