﻿/********************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;

using Parliament.Dto;
using Parliament.Repository;
using Parliament.Model;
using Parliament.Exceptions;

using Microsoft.Practices.Unity;
/********************************************************************************************/


namespace Parliament.Service.Impl
{
    public class UserService : IUserService
    {
        [ Dependency ]
        protected IUserRepository userRepository { set; get; }

        [ Dependency ]
        protected IRegisteredDocumentCreatorRepository registeredDcRepository { set; get; }

/*------------------------------------------------------------------------------------------*/

        public UserService (
                IUserRepository _userRepository
            ,   IRegisteredDocumentCreatorRepository _registeredDcRepository
        )
        {
            userRepository          = _userRepository;
            registeredDcRepository  = _registeredDcRepository;
        }

/*------------------------------------------------------------------------------------------*/

        public void ChangeEmail ( Guid _accountId, string _newEmail )
        {
            User user = resolveUser( _accountId );

            if ( user.Email == _newEmail)
                throw new DuplicateNamedEntityException( typeof( Administrator ), _newEmail );

            user.Email = _newEmail;
        }

/*------------------------------------------------------------------------------------------*/

        public void ChangeName ( Guid _accountId, string _newName )
        {
            User user = resolveUser( _accountId );
            user.Name = _newName;
        }

/*------------------------------------------------------------------------------------------*/

        public void ChangePassword ( Guid _accountId, string _oldPassword, string _newPassword )
        {
            User user = resolveUser( _accountId );

            if ( !user.checkPassword( _oldPassword ) )
                throw AccountException.FailPassword( _accountId, "User" );

            user.Password = _newPassword;
        }

/*------------------------------------------------------------------------------------------*/

        public Guid createNew ()
        {
            // NOTE: Will be established by use of UserService.
            User newUser = new User (
                    Guid.NewGuid()
                ,   "" /* _name */
                ,   "" /* _password */
                ,   "" /* _email */
                ,   User.AccountStatus.None
                ,   User.UserPost.None
                ,   "" /* _imageUrl */ 
            );

            userRepository.Add( newUser );

            return newUser.DomainId;
        }

/*------------------------------------------------------------------------------------------*/

        public Guid createNew ( string _email, string _password, string _name, string _userPost, string _imageUrl )
        {
            User user = userRepository.FindByEmail( _email );
            if ( user != null )
                throw new DuplicateNamedEntityException( typeof ( User ), _email );

            User newUser = new User (
                    Guid.NewGuid()
                ,    _name
                ,   _password
                ,   _email
                ,   User.AccountStatus.Act
                ,   ( User.UserPost )Enum.Parse( typeof( User.UserPost ), _userPost )
                ,   _imageUrl
            );

            userRepository.Add(newUser);

            return newUser.DomainId;
        }

/*------------------------------------------------------------------------------------------*/

        public void editAccountStatus ( Guid _accountId, string _newStatus )
        {
            User user = resolveUser( _accountId );

            if ( _newStatus == Account.AccountStatus.ConcludedPowers.ToString() )
                throw AccountLifeCycleException.AccountConcludedPowers(_accountId);

            user.Status = ( Account.AccountStatus )Enum.Parse( 
                    typeof( Account.AccountStatus )
                ,   _newStatus
            );
        }

/*------------------------------------------------------------------------------------------*/

        public UserDto Identify ( string _email, string _password )
        {
            User user = userRepository.FindByEmail( _email );
            if ( user == null )
                return null;

            if ( !user.checkPassword( _password ) )
                throw AccountException.FailPassword( user.DomainId, "User" );

            return user.toDto();
        }

/*------------------------------------------------------------------------------------------*/

        public UserDto View ( Guid _domainId )
        {
            return resolveUser( _domainId ).toDto();
        }

/*------------------------------------------------------------------------------------------*/

        public IList< Guid > ViewAll ()
        {
            return userRepository.SelectAllDomainIds().ToList();
        }

/*------------------------------------------------------------------------------------------*/

        public IList< BillDto > ViewAssociatedBills ( Guid _accountId )
        {
            User user = resolveUser( _accountId );
            RegisteredDocumentCreator creator = registeredDcRepository.FindRegisteredDcByUser( user );

            if ( creator == null )
                throw new ServiceUnresolvedEntityException( 
                        typeof( RegisteredDocumentCreator )
                    ,   _accountId 
                );

            List< BillDto > result = new List< BillDto >();
            int documentsCount = creator.CreatedDocumentCount;

            for( int i = 0; i < documentsCount; ++i )
            {
                Document document = creator.getCreatedDocument( i );

                if ( document is Bill )
                    result.Add( ( document as Bill ).toDto() );
            }

            return result;
        }

/*------------------------------------------------------------------------------------------*/

        public IList< LawDto > ViewAssociatedLaws( Guid _accountId )
        {
            User user = resolveUser( _accountId );
            RegisteredDocumentCreator creator = registeredDcRepository.FindRegisteredDcByUser( user );

            if ( creator == null )
                throw new ServiceUnresolvedEntityException( 
                        typeof( RegisteredDocumentCreator )
                    ,   _accountId 
                );

            List< LawDto > result = new List< LawDto >();
            int documentsCount = creator.CreatedDocumentCount;

            for ( int i = 0; i < documentsCount; ++i )
            {
                Document document = creator.getCreatedDocument( i );
                if ( document is Law )
                    result.Add( ( document as Law ).toDto() );
            }

            return result;
        }

/*------------------------------------------------------------------------------------------*/

        private User resolveUser( Guid _userId )
        {
            return ServiceUtils.ResolveEntity( userRepository, _userId );
        }
    }
}
