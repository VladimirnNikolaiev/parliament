﻿/********************************************************************************************/
using Parliament.Dto;
using Parliament.Model;
using System;
using System.Collections.Generic;
/********************************************************************************************/


namespace Parliament.Service.Impl
{
    static class DtoBuilder
    {
        public static AdministratorDto toDto ( this Administrator _administrator )
        {
            return new AdministratorDto (
                    _administrator.DomainId
                ,   _administrator.Name
                ,   _administrator.Email
                ,   _administrator.AccessRight.ToString()
                ,   _administrator.Status.ToString()
                ,   _administrator.ImageUrl
           );
        }

/*------------------------------------------------------------------------------------------*/

        public static UserDto toDto ( this User _user )
        {
            return new UserDto (
                    _user.DomainId
                ,   _user.Name
                ,   _user.Email
                ,   _user.Post.ToString()
                ,   _user.Status.ToString()
                ,   _user.ImageUrl
           );
        }

/*------------------------------------------------------------------------------------------*/

        public static RegisteredDocumentCreatorDto toDto ( this RegisteredDocumentCreator _creator )
        {
            List< Guid > documents = new List< Guid >();

            foreach (var document in _creator.Documents)
                documents.Add( document.DomainId );

            return new RegisteredDocumentCreatorDto (
                    _creator.DomainId
                 ,  ( _creator.Account as User ).toDto()
                 ,  documents
            );
        }

/*------------------------------------------------------------------------------------------*/

        public static UnregisteredDocumentCreatorDto toDto( this UnregisteredDocumentCreator _creator )
        {
            List<Guid> documents = new List<Guid>();

            foreach ( var document in _creator.Documents )
                documents.Add( document.DomainId );

            return new UnregisteredDocumentCreatorDto (
                    _creator.DomainId
                ,   _creator.Name
                ,   _creator.Role.ToString()
                ,   documents
            );
        }

/*------------------------------------------------------------------------------------------*/

        public static RevisionDto toDto ( this Revision _revision )
        {
            List< Guid > creators = new List< Guid >();

            foreach( var creator in _revision.Creators )
                creators.Add( creator.DomainId );

            return new RevisionDto (
                    _revision.DomainId
                ,   _revision.RevisionInfo.Document.DocumentPath
                ,   _revision.CreationDate
                ,   creators
            );
        }

/*------------------------------------------------------------------------------------------*/

        public static BillDto toDto ( this Bill _bill )
        {
            List< RevisionDto > revisions = new List< RevisionDto >();

            foreach ( var revision in _bill.Revisions )
                revisions.Add( revision.toDto() );

            return new BillDto (
                    _bill.DomainId
                ,   _bill.Name
                ,   _bill.Status.ToString()
                ,   revisions
           );
        }

/*------------------------------------------------------------------------------------------*/

        public static LawDto toDto ( this Law _law )
        {
            List< RevisionDto > revisions = new List< RevisionDto >();

            foreach ( var revision in _law.Revisions )
                revisions.Add( revision.toDto() );

            return new LawDto (
                    _law.DomainId
                ,   _law.Name
                ,   _law.Status.ToString()
                ,   revisions
            );
        }
    }
}


/********************************************************************************************/