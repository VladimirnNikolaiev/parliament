﻿/********************************************************************************************/
using Parliament.Dto;
/********************************************************************************************/


namespace Parliament.Web.Models
{
    public class AdministratorViewModel
    {
        public AdministratorDto Administrator { get; set; }

        // Some more methods from UserStories ( Report №5 ) ?
    }
}


/********************************************************************************************/