﻿/********************************************************************************************/


namespace Parliament.Web.Models
{
    public class ErrorViewModel
    {
        public string ErrorTitle { get; set; }
        public string ErrorMessage { get; set; }
    }
}


/********************************************************************************************/