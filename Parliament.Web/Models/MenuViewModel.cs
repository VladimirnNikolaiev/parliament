﻿/********************************************************************************************/
using System.Collections.Generic;

using Parliament.Dto;
/********************************************************************************************/


namespace Parliament.Web.Models
{
    public class MenuViewModel
    {
        // NOTE: Maybe create basic functional for a primitive adding accounts ? 
        public IList< AdministratorDto > Administrators { get; set; }
        public IList< UserDto > Users { get; set; }

        public IList< LawDto > Laws { get; set; }

        public IList< BillDto > Bills { get; set; }
    }
}


/********************************************************************************************/