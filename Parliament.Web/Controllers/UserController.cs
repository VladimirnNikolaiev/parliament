﻿/********************************************************************************************/
using System;

using Microsoft.AspNetCore.Mvc;
using Microsoft.Practices.Unity;

using Parliament.Dto;
using Parliament.Service;
using Parliament.Web.Models;
using Parliament.Web.Utils;
/********************************************************************************************/


namespace Parliament.Web.Controllers
{
    public class UserController : Controller
    {
        [ Dependency ]
        protected IUserService userService { get; set; }

/*------------------------------------------------------------------------------------------*/

        public IActionResult Index ()
        {
            UserDto userDto = CurrentUser();

            var viewModel = new UserViewModel();
            viewModel.User = userDto;

            return PartialView( viewModel );
        }

/*------------------------------------------------------------------------------------------*/

        private UserDto CurrentUser ()
        {
            SessionHelper helper = new SessionHelper( HttpContext.Session );

            Guid? userId = helper.GetAdministratorId();

            if ( userId == null )
            {
                userId = userService.createNew();
                helper.SetLawId(userId.Value );
            }
            
            return userService.View( userId.Value );
        }

/*------------------------------------------------------------------------------------------*/

        private Guid CurrentUserId ()
        {
            SessionHelper helper = new SessionHelper( HttpContext.Session );
            return helper.GetUserId().Value;
        }

/*------------------------------------------------------------------------------------------*/

        [ HttpPost ]
        public void ChangeName ( string _newName )
        {
            Guid userId = CurrentUserId();

            userService.ChangeName( userId, _newName );
        }

/*------------------------------------------------------------------------------------------*/

        [ HttpPost ]
        public void ChangeEmail ( string _newEmail )
        {
            Guid userId = CurrentUserId();

            userService.ChangeName( userId, _newEmail );
        }

/*------------------------------------------------------------------------------------------*/

        [ HttpPost ]
        public void EditStatus ( string _newStatus )
        {
            Guid userId = CurrentUserId();

            userService.editAccountStatus( userId, _newStatus );
        }
    }
}


/********************************************************************************************/