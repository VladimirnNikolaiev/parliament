﻿/********************************************************************************************/
using System;

using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Mvc;

using Parliament.Web.Models;

/********************************************************************************************/


namespace Parliament.Web.Controllers
{
    public class HomeController : Controller
    {

        public IActionResult Index() => new RedirectToActionResult( "Index", "Menu", null );

/*------------------------------------------------------------------------------------------*/
        
        [ HttpPost ]
        public void SetLocale ( string _language )
        {
            string culture = "en-US";

            if ( _language == "ru" )
                culture = "ru-RU";
            else if ( _language == "uk" )
                culture = "uk-UA";

            Response.Cookies.Append(
                    CookieRequestCultureProvider.DefaultCookieName
                ,   CookieRequestCultureProvider.MakeCookieValue( new RequestCulture( culture ) )
                ,   new CookieOptions { Expires = DateTimeOffset.UtcNow.AddYears( 1 ) }
            );
        }
        
/*------------------------------------------------------------------------------------------*/

         public IActionResult FatalError ()
        {
            var feature = HttpContext.Features.Get< IExceptionHandlerFeature >();
            var error = feature?.Error;

            var errorViewModel = new ErrorViewModel();
            errorViewModel.ErrorTitle = "500 Fatal Server Error";
            errorViewModel.ErrorMessage = error.Message;

            return View( "Error", errorViewModel );
        }

/*------------------------------------------------------------------------------------------*/

        public IActionResult Error404 ( string errorPath )
        {
            var errorViewModel = new ErrorViewModel();
            errorViewModel.ErrorTitle = "404";
            errorViewModel.ErrorMessage = string.Format( "The requested page {0} was not found", errorPath );

            return View( "Error", errorViewModel );
        }
    }
}


/********************************************************************************************/