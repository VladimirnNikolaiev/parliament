﻿/********************************************************************************************/
using System;

using Microsoft.AspNetCore.Mvc;
using Microsoft.Practices.Unity;

using Parliament.Dto;
using Parliament.Service;
using Parliament.Web.Models;
using Parliament.Web.Utils;
/********************************************************************************************/


namespace Parliament.Web.Controllers
{
    public class LawController : Controller
    {
        [ Dependency ]
        protected ILawService lawService { get; set; }

/*------------------------------------------------------------------------------------------*/

        public IActionResult Index()
        {
            LawDto lawDto = CurrentLaw();

            var viewModel = new LawViewModel();
            viewModel.Law = lawDto;

            return PartialView( viewModel );
        }

/*------------------------------------------------------------------------------------------*/

        private LawDto CurrentLaw ()
        {
            SessionHelper helper = new SessionHelper( HttpContext.Session );

            Guid? lawId = helper.GetLawId();

            if ( lawId == null )
            {
                lawId = lawService.createNew();
                helper.SetLawId( lawId.Value );
            }

            // TODO: Fix ViewFullInformation for DocService.
            return lawService.ViewFullInformation( lawId.Value );
        }

/*------------------------------------------------------------------------------------------*/

        private Guid CurrentLawId ()
        {
            SessionHelper helper = new SessionHelper( HttpContext.Session );
            return helper.GetLawId().Value;
        }

/*------------------------------------------------------------------------------------------*/

        [ HttpPost ]
        public void AddRevision ( Guid _revisionId, string _docFilePath )
        {
            lawService.addRevision(
                    _revisionId
                ,   _docFilePath
                ,   DateTime.Today
                ,   null /*Not supporting authors for current iteration.*/
            );
        }

/*------------------------------------------------------------------------------------------*/

        [ HttpPost ]
        public void EditStatus ( string _newStatus )
        {
            Guid lawId = CurrentLawId();

            lawService.editStatus( lawId, _newStatus );
        }
    }
}


/********************************************************************************************/