﻿/********************************************************************************************/
using System;

using Microsoft.AspNetCore.Mvc;
using Microsoft.Practices.Unity;

using Parliament.Dto;
using Parliament.Service;
using Parliament.Web.Models;
using Parliament.Web.Utils;
/********************************************************************************************/


namespace Parliament.Web.Controllers
{
    public class AdministratorController : Controller
    {
        [ Dependency ]
        protected IAdministratorService administratorService { get; set; }

/*------------------------------------------------------------------------------------------*/

        public IActionResult Index ()
        {
            AdministratorDto administratorDto = CurrentAdministrator();

            var viewModel = new AdministratorViewModel();
            viewModel.Administrator = administratorDto;

            return PartialView( viewModel );
        }

/*------------------------------------------------------------------------------------------*/

        private AdministratorDto CurrentAdministrator ()
        {
            SessionHelper helper = new SessionHelper( HttpContext.Session );

            Guid? administratorId = helper.GetAdministratorId();

            if ( administratorId == null )
            {
                administratorId = administratorService.createNew();
                helper.SetLawId( administratorId.Value );
            }
            
            return administratorService.View( administratorId.Value );
        }

/*------------------------------------------------------------------------------------------*/

        private Guid CurrentAdministratorId ()
        {
            SessionHelper helper = new SessionHelper( HttpContext.Session );
            return helper.GetAdministratorId().Value;
        }

/*------------------------------------------------------------------------------------------*/

        [ HttpPost ]
        public void ChangeName ( string _newName )
        {
            Guid administratorId = CurrentAdministratorId();

            administratorService.ChangeName( administratorId, _newName );
        }

/*------------------------------------------------------------------------------------------*/

        [ HttpPost ]
        public void ChangeEmail ( string _newEmail )
        {
            Guid administratorId = CurrentAdministratorId();

            administratorService.ChangeName( administratorId, _newEmail );
        }

/*------------------------------------------------------------------------------------------*/

        [ HttpPost ]
        public void EditStatus ( string _newStatus )
        {
            Guid administratorId = CurrentAdministratorId();

            administratorService.editAccountStatus( administratorId, _newStatus );
        }
    }
}


/********************************************************************************************/