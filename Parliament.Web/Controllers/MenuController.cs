﻿/********************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;

using Microsoft.AspNetCore.Mvc;
using Microsoft.Practices.Unity;

using Parliament.Dto;
using Parliament.Service;
using Parliament.Web.Models;
/********************************************************************************************/


namespace Parliament.Web.Controllers
{
    public class MenuController : Controller
    {
        [ Dependency ]
        protected ILawService lawService { get; set; }

        [ Dependency ]
        protected IBillService billService { get; set; }

        [ Dependency ]
        protected IAdministratorService administratorService { get; set; }

        [ Dependency ]
        protected IUserService userService { get; set; }

/*------------------------------------------------------------------------------------------*/

        public IActionResult Index()
        {
            var viewModel = new MenuViewModel();

            viewModel.Laws = GetLawsList();
            viewModel.Bills = GetBillsList();
            viewModel.Administrators = GetAdministratorsList();
            viewModel.Users = GetUsersList();

            return View( viewModel );
        }

/*------------------------------------------------------------------------------------------*/

        private IList< LawDto > GetLawsList ()
        {
            IList< Guid > lawIds = lawService.ViewAll();

            IList< LawDto > result = new List< LawDto >();

            // TODO: Fix ViewFullInformation for DocService.
            foreach ( var lawId in lawIds )
                result.Add( lawService.ViewFullInformation( lawId ) );

            return result.OrderBy( p => p.Name ).ToList();
        }

/*------------------------------------------------------------------------------------------*/

        private IList< BillDto > GetBillsList ()
        {
            IList< Guid > billIds = billService.ViewAll();

            IList< BillDto > result = new List< BillDto >();

            // TODO: Fix ViewFullInformation for DocService.
            foreach ( var billId in billIds )
                result.Add( billService.ViewFullInformation( billId ) );

            return result.OrderBy( p => p.Name ).ToList();
        }

/*------------------------------------------------------------------------------------------*/

       private IList< AdministratorDto > GetAdministratorsList ()
       {
            IList< Guid > administratorIds = administratorService.ViewAll();

            IList< AdministratorDto > result = new List< AdministratorDto >();

            foreach ( var administratorId in administratorIds )
                result.Add( administratorService.View( administratorId ) );

            return result.OrderBy( p => p.Name ).ToList();
       }

/*------------------------------------------------------------------------------------------*/

        private IList< UserDto > GetUsersList ()
        {
            IList< Guid > userIds = administratorService.ViewAll();

            IList< UserDto > result = new List< UserDto >();

            foreach ( var userId in userIds )
                result.Add( userService.View( userId ) );

            return result.OrderBy( p => p.Name ).ToList();
        }
    }
}


/********************************************************************************************/