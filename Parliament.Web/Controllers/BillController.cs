﻿/********************************************************************************************/
using System;

using Microsoft.AspNetCore.Mvc;
using Microsoft.Practices.Unity;

using Parliament.Dto;
using Parliament.Service;
using Parliament.Web.Models;
using Parliament.Web.Utils;
/********************************************************************************************/


namespace Parliament.Web.Controllers
{
    public class BillController : Controller
    {
        [ Dependency ]
        protected IBillService billService { get; set; }

/*------------------------------------------------------------------------------------------*/

        public IActionResult Index()
        {
            BillDto billDto = CurrentBill();

            var viewModel = new BillViewModel();
            viewModel.Bill = billDto;

            return PartialView( viewModel );
        }

/*------------------------------------------------------------------------------------------*/

        private BillDto CurrentBill ()
        {
            SessionHelper helper = new SessionHelper( HttpContext.Session );

            Guid? billId = helper.GetBillId();

            if ( billId == null )
            {
                billId = billService.createNew();
                helper.SetBillId( billId.Value );
            }

            // TODO: Fix ViewFullInformation for DocService.
            return billService.ViewFullInformation( billId.Value );
        }

/*------------------------------------------------------------------------------------------*/

        private Guid CurrentBillId ()
        {
            SessionHelper helper = new SessionHelper( HttpContext.Session );
            return helper.GetBillId().Value;
        }

/*------------------------------------------------------------------------------------------*/

        [ HttpPost ]
        public void AddRevision ( Guid _revisionId, string _docFilePath )
        {
            billService.addRevision(
                    _revisionId
                ,   _docFilePath
                ,   DateTime.Today
                ,   null /*Not supporting authors for current iteration.*/
            );
        }

/*------------------------------------------------------------------------------------------*/

        [ HttpPost ]
        public void EditStatus ( string _newStatus )
        {
            Guid billId = CurrentBillId();

            billService.editStatus( billId, _newStatus );
        }
    }
}


/********************************************************************************************/