﻿/********************************************************************************************/
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Controllers;
using Microsoft.Practices.Unity;
/********************************************************************************************/


namespace Parliament.Web.Utils
{
    public class UnityControllerActivator : IControllerActivator
    {
        private IUnityContainer unityContainer;

/*------------------------------------------------------------------------------------------*/

        public UnityControllerActivator( IUnityContainer container )
        {
            unityContainer = container;
        }

/*------------------------------------------------------------------------------------------*/

        #region Implementation of IControllerActivator

        public object Create( ControllerContext context )
        {
            return unityContainer.Resolve( context.ActionDescriptor.ControllerTypeInfo.AsType() );
        }

        public void Release( ControllerContext context, object controller )
        {
            // Ignored.
        }

        #endregion
    }
}


/********************************************************************************************/