﻿/********************************************************************************************/
using System;

using Microsoft.AspNetCore.Http;
/********************************************************************************************/


namespace Parliament.Web.Utils
{
    public class SessionHelper
    {
        private static readonly string LawId = "LAW_ID";
        private static readonly string BillId = "BILL_ID";

        private static readonly string UserId = "USER_ID";
        private static readonly string AdministratorId = "ADMINISTRATOR_ID";

        private ISession session;

/*------------------------------------------------------------------------------------------*/

        public SessionHelper ( ISession session )
        {
            this.session = session;
        }

/*------------------------------------------------------------------------------------------*/

        public Guid? GetLawId ()
        {
            string result = session.GetString( LawId );
            if ( result == null || result.Length == 0 )
                return null;

            return Guid.Parse( result );
        }

/*------------------------------------------------------------------------------------------*/

        public void SetLawId ( Guid lawId )
        {
            session.SetString( LawId, lawId.ToString() );
        }

/*------------------------------------------------------------------------------------------*/

        public void ResetCartId ()
        {
            session.Remove( LawId );
        }

/*------------------------------------------------------------------------------------------*/

        public Guid? GetBillId ()
        {
            string result = session.GetString( BillId );
            if ( result == null || result.Length == 0 )
                return null;

            return Guid.Parse( result );
        }

/*------------------------------------------------------------------------------------------*/

        public void SetBillId ( Guid billId )
        {
            session.SetString( BillId, billId.ToString() );
        }

/*------------------------------------------------------------------------------------------*/

        public void ResetBillId ()
        {
            session.Remove( BillId );
        }

/*------------------------------------------------------------------------------------------*/

        public Guid? GetUserId ()
        {
            string result = session.GetString( UserId );
            if ( result == null || result.Length == 0 )
                return null;

            return Guid.Parse( result );
        }

/*------------------------------------------------------------------------------------------*/

        public void SetUserlId ( Guid userId )
        {
            session.SetString( UserId, userId.ToString() );
        }

/*------------------------------------------------------------------------------------------*/

        public void ResetUserId ()
        {
            session.Remove( UserId );
        }

/*------------------------------------------------------------------------------------------*/

        public Guid? GetAdministratorId ()
        {
            string result = session.GetString( AdministratorId );
            if ( result == null || result.Length == 0 )
                return null;

            return Guid.Parse( result );
        }

/*------------------------------------------------------------------------------------------*/

        public void SetAdministratorlId( Guid administratorId )
        {
            session.SetString( AdministratorId, administratorId.ToString() );
        }

/*------------------------------------------------------------------------------------------*/

        public void ResetAdministratorId()
        {
            session.Remove( AdministratorId );
        }
    }
}


/********************************************************************************************/