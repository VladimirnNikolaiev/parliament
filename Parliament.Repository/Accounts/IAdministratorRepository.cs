﻿/********************************************************************************************/
using System;
using System.Linq;
using Parliament.Model;
/********************************************************************************************/


namespace Parliament.Repository
{
    public interface IAdministratorRepository : IAccountRepository< Administrator >
    {
        IQueryable< Guid > SelectDocumentManagementAdminsByIds ();

        IQueryable< Guid > SelectAccountsManagementAdminsByIds ();

        IQueryable< Guid > SelectFullRigthsAdminsByIds ();
    }
}


/********************************************************************************************/