﻿/********************************************************************************************/
using System;
/********************************************************************************************/


namespace Parliament.Repository
{
    public interface IAccountRepository < T > : INamedObjectRepository< T > where T : Model.Account
    {
        T FindById ( Guid _id );

        T FindByEmail ( string _email );
    }
}


/********************************************************************************************/