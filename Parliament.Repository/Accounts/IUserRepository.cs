﻿/********************************************************************************************/
using System;
using System.Linq;
using Parliament.Model;
/********************************************************************************************/


namespace Parliament.Repository
{
    public interface IUserRepository : IAccountRepository< User >
    {
        IQueryable< Guid > SelectDepytiesByIds ();

        IQueryable< Guid > SelectSpeakersByIds ();
    }
}


/********************************************************************************************/