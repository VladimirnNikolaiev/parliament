﻿/********************************************************************************************/


namespace Parliament.Repository
{
    public interface INamedObjectRepository< T > : IRepository< T > where T : Utils.Entity
    {
       T FindByName ( string _name );
    }
}


/********************************************************************************************/
