﻿/********************************************************************************************/
using System;
using System.Linq;
using Parliament.Model;
/********************************************************************************************/


namespace Parliament.Repository
{
    public interface IUnregisteredDocumentCreatorRepository : INamedObjectRepository< UnregisteredDocumentCreator >
    {
        UnregisteredDocumentCreator FindUnregisteredDCById ( Guid _unregisteredDCId );

        IQueryable< Guid > SelectPresidentsAsUnregisteredDC ();

        IQueryable< Guid > SelectPrimeMinistersAsUnregisteredDC ();

        IQueryable< Guid > SelectVicePrimeMinistersAsUnregisteredDC ();

        IQueryable< Guid > SelectMinistersOfInternalAffairsAsUnregisteredDC ();

        IQueryable< Guid > SelectMinistersOfJusticeAsUnregisteredDC ();

        IQueryable< Guid > SelectMinistersOfFinancesAsUnregisteredDC ();

        IQueryable< Guid > SelectMinistersOfEducationAsUnregisteredDC ();

        IQueryable< Guid > SelectMinistersOfHealthAsUnregisteredDC ();

        IQueryable< Guid > SelectMinistersOfSocialPolicyAsUnregisteredDC ();

        IQueryable< Guid > SelectMinisterSOfEcologyAsUnregisteredDC ();

        IQueryable< Guid > SelectMinistersOfCultureAsUnregisteredDC ();

        IQueryable< Guid > SelectMinistersOfForeignAffairsAsUnregisteredDC ();

        IQueryable< Guid > SelectMinisterOfAgrarianPolicyAsUnregisteredDC ();

        IQueryable< Guid > SelectMinistersOfFuelAndEnergyAsUnregisteredDC ();

        IQueryable< Guid > SelectMinistersOfYouthAndSportsAsUnregisteredDC ();

        IQueryable< Guid > SelectMinistersOfInrastructureAsUnregisteredDC ();

        IQueryable< Guid > SelectMinistersOfDefenceAsUnregisteredDC ();

        IQueryable< Guid > SelectPublicFiguresAsUnregisteredDC ();
    }
}


/********************************************************************************************/