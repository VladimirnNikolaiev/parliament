﻿/********************************************************************************************/
using System;
using Parliament.Model;
/********************************************************************************************/


namespace Parliament.Repository
{
    public interface IRevisionRepository : IRepository< Revision >
    {
        Revision FindRevisionById ( Guid _revisionId );

        Revision FindRevisionByCreationDate ( DateTime _creationDate );

        Revision FindRevisionByMajorCreator ( DocumentCreator _majorCreator );
    }
}


/********************************************************************************************/
