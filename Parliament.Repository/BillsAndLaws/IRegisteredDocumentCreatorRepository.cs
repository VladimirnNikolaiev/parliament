﻿/********************************************************************************************/
using System;
using Parliament.Model;
/********************************************************************************************/


namespace Parliament.Repository
{
    public interface IRegisteredDocumentCreatorRepository : INamedObjectRepository< RegisteredDocumentCreator >
    {
        RegisteredDocumentCreator FindRegisteredDCById ( Guid _registeredDCId );

        RegisteredDocumentCreator FindRegisteredDcByUser ( User _user );
    }
}


/********************************************************************************************/