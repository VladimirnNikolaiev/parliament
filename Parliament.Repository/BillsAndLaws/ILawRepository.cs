﻿/********************************************************************************************/
using System;
using System.Linq;
using Parliament.Model;
/********************************************************************************************/


namespace Parliament.Repository
{
    public interface ILawRepository : INamedObjectRepository< Law >
    {
        Law FindLawById ( Guid _lawId );

        IQueryable< Guid > SelectActLawsByIds ();

        IQueryable< Guid > SelectRemovedLawsByIds ();
    }
}


/********************************************************************************************/