﻿/********************************************************************************************/
using System;
using System.Linq;
using Parliament.Model;
/********************************************************************************************/


namespace Parliament.Repository
{
    public interface IBillRepository : INamedObjectRepository< Bill >
    {
        Bill FindBillById ( Guid _billId );

        IQueryable< Guid > SelectNewBillsByIds ();

        IQueryable< Guid > SelectComesIntoForceBillsByIds ();

        IQueryable< Guid > SelectAwaitsPresidentsSignatureBillsByIds ();

        IQueryable< Guid > SelectVetoedBillsByIds ();

        IQueryable< Guid > SelectVotedAsAWholeBillsByIds ();

        IQueryable< Guid > SelectVotedIn1stReadingBillsByIds ();

        IQueryable< Guid > SelectVotedIn2stReadingBillsByIds ();

        IQueryable< Guid > SelectVotedIn3stReadingBillsByIds ();

        IQueryable< Guid > SelectIncludedToAgendaBillsByIds ();
    }
}


/********************************************************************************************/