﻿/********************************************************************************************/
using System;
using System.Linq;
/********************************************************************************************/


namespace Parliament.Repository
{
    public interface IRepository < T > where T : Utils.Entity
    {
        void StartTransaction ();

        void Commit ();

        void Rollback ();

        int Count ();

        T Load ( int id );

        IQueryable< T > LoadAll ();

        void Add ( T _t );

        void Delete ( T _t );

        T FindByDomainId ( Guid domainId );

        IQueryable< Guid > SelectAllDomainIds ();
    }
}


/********************************************************************************************/