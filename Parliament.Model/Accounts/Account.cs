﻿/********************************************************************************************/
using System;
using Parliament.Exceptions;
/********************************************************************************************/


namespace Parliament.Model
{
    public abstract class Account : Utils.Entity
    {
        public enum AccountStatus
        {
                None = 0    /* Default value */

            ,   Act
            ,   ConcludedPowers
        }

/*------------------------------------------------------------------------------------------*/

        public string Name { get; set; }

        public string Email { get; set; }

        public string Password { get; set; }

        public string ImageUrl { get; set; }

        public AccountStatus Status { get; set; }

/*------------------------------------------------------------------------------------------*/

        protected Account () {}

/*------------------------------------------------------------------------------------------*/

        protected Account (
                Guid _id
            ,   string _name
            ,   string _password
            ,   string _email
            ,   AccountStatus _status
            ,   string _imageUrl
        )
            :   base ( _id )
        {
            Name       = _name;
            Password   = _password;
            Email      = _email;
            ImageUrl   = _imageUrl;

            if ( _status == AccountStatus.ConcludedPowers )
                throw AccountLifeCycleException.AccountConcludedPowers( _id );

            Status = _status;
        }

/*------------------------------------------------------------------------------------------*/

        public bool checkPassword ( string _password )
        {
            return Password == _password;
        }

/*------------------------------------------------------------------------------------------*/

        private void changePassword ( string _password )
        {
            Password = _password;
        }

/*------------------------------------------------------------------------------------------*/

        public override string ToString ()
        {
            return string.Format (
                    "\nID = {0}\nName = {1}\nEmail = {2}\nPassword = {3}"
                ,   DomainId
                ,   Name
                ,   Email
                ,   Password
            );
        }
    }
}


/********************************************************************************************/