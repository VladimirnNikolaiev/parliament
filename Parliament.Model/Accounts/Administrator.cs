﻿/********************************************************************************************/
using System;
/********************************************************************************************/


namespace Parliament.Model
{
    public class Administrator : Account
    {
        public enum AdministratorAccessRight
        {
                None = 0    /* Default value */

            ,   DocumentManagement
            ,   AccountsManagement
            ,   Full
        }

/*------------------------------------------------------------------------------------------*/

        public AdministratorAccessRight AccessRight { get; private set; }

/*------------------------------------------------------------------------------------------*/

        protected Administrator () {}

/*------------------------------------------------------------------------------------------*/

        public Administrator (
                Guid _id
            ,   string _name 
            ,   string _password
            ,   string _email
            ,   AccountStatus _status
            ,   AdministratorAccessRight _right
            ,   string _imageUrl
        )
            :   base ( _id, _name, _password, _email, _status, _imageUrl )
        {
            AccessRight = _right;
        }

/*------------------------------------------------------------------------------------------*/

        public override string ToString ()
        {
            return base.ToString() + "\nRigths = " + AccessRight.ToString();
        }
    }
}


/********************************************************************************************/