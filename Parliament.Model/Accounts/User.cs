﻿/********************************************************************************************/
using System;
/********************************************************************************************/


namespace Parliament.Model
{
    public class User : Account
    {
        public enum UserPost
        {
                None = 0    /* Default value. */

            ,   Deputy
            ,   Speaker
        }

/*------------------------------------------------------------------------------------------*/

        public UserPost Post { get; private set; }

/*------------------------------------------------------------------------------------------*/

        protected User () {}

/*------------------------------------------------------------------------------------------*/

        public User (
                Guid _id
            ,   string _name
            ,   string _password
            ,   string _email
            ,   AccountStatus _status
            ,   UserPost _post
            ,   string _imageUrl
        )
            :   base ( _id, _name, _password, _email, _status, _imageUrl )
        {
            Post = _post;
        }

/*------------------------------------------------------------------------------------------*/

        public override string ToString ()
        {
            return "Post: " + Post.ToString() + base.ToString(); 
        }
    }
}


/********************************************************************************************/