﻿/********************************************************************************************/
using System;
/********************************************************************************************/


namespace Parliament.Model
{
    public class Session : Utils.Entity
    {
        public enum SessionStatus
        {
                Opened
            ,   Closed
        } 
        
/*------------------------------------------------------------------------------------------*/         

        public virtual Account Account { get; private set; }
        public DateTime OpenTime { get; private set; }
        public DateTime CloseTime { get; private set; }

/*------------------------------------------------------------------------------------------*/

        public SessionStatus Status
        {
            get
            {
                if ( CloseTime == null )
                    return SessionStatus.Opened;
                else
                    return SessionStatus.Closed;
            }
        }

/*------------------------------------------------------------------------------------------*/

        protected Session () {}

/*------------------------------------------------------------------------------------------*/

        public Session ( Guid _id, Account _account )
            :   base( _id )
        {
            if ( _account == null )
                throw new ArgumentNullException( "Empty account" );

            Account = _account;
            OpenTime = DateTime.Now;
        }

/*------------------------------------------------------------------------------------------*/

        public void closeSession ()
        {
            CloseTime = DateTime.Now;
        }

/*------------------------------------------------------------------------------------------*/

        public override string ToString ()
        {
            return "Session StartTime: " + OpenTime.ToString() + Account.ToString();
        }
    }
}


/********************************************************************************************/