﻿/********************************************************************************************/
using System;
/********************************************************************************************/


namespace Parliament.Model
{
    public class RegisteredDocumentCreator : DocumentCreator
    {
        public virtual Account Account { get; private set; }

/*------------------------------------------------------------------------------------------*/

        protected RegisteredDocumentCreator () {}

/*------------------------------------------------------------------------------------------*/

        public RegisteredDocumentCreator ( Guid _id, Account _account )
            :   base( _id )
        {
            if ( _account == null )
                throw new ArgumentNullException( "Empty account" );

            Account = _account;
        }

/*------------------------------------------------------------------------------------------*/

        public override bool isRegisteredUser ()
        {
            return true;
        }

/*------------------------------------------------------------------------------------------*/

        public override Account getAccount ()
        {
            return Account;
        }

/*------------------------------------------------------------------------------------------*/

        public override string getName ()
        {
            return Account.Name;
        }
    }
}


/********************************************************************************************/