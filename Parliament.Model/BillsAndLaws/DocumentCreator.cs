﻿/********************************************************************************************/
using System;
using System.Collections.Generic;

using Parliament.Exceptions;
/********************************************************************************************/


namespace Parliament.Model
{
    public abstract class DocumentCreator : Utils.Entity
    {
        public virtual IList< Document > Documents { get; private set; }

/*------------------------------------------------------------------------------------------*/

        public int CreatedDocumentCount
        {
            get { return Documents.Count; }
        }

/*------------------------------------------------------------------------------------------*/

        protected DocumentCreator () {}

/*------------------------------------------------------------------------------------------*/

        protected DocumentCreator ( Guid _id )
            :   base( _id )
        {
            Documents = new List<Document>();
        }

/*------------------------------------------------------------------------------------------*/

        public Document getCreatedDocument ( int _index )
        {
            return Documents[ _index ];
        }

/*------------------------------------------------------------------------------------------*/

        public void addCreatedDocument ( Document _document )
        {
            if( _document == null )
                throw new ArgumentNullException( "Empty document" );

            if ( Documents.Contains( _document ) )
                throw DocumentException.SameDocument( _document.DomainId );

            Documents.Add( _document );
        }

/*------------------------------------------------------------------------------------------*/

        public override string ToString()
        {
            return getName();
        }

/*------------------------------------------------------------------------------------------*/

        abstract public bool isRegisteredUser();
        public abstract Account getAccount();

        public abstract string getName();
    }
}


/********************************************************************************************/