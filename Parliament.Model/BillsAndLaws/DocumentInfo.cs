﻿
namespace Parliament.Model
{
    public class DocumentInfo
    {
        public DocumentText Document { get; private set; }

/*------------------------------------------------------------------------------------------*/

        public string DocumentText
        {
            get { return Document.asString(); }
        }

/*------------------------------------------------------------------------------------------*/

        private DocumentInfo () {}

/*------------------------------------------------------------------------------------------*/

        public DocumentInfo ( string _path )
        {
            Document = new DocumentText( _path );
        }
    }
}


/********************************************************************************************/