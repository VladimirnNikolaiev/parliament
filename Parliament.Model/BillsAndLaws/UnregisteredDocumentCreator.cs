﻿/********************************************************************************************/
using System;
/********************************************************************************************/


namespace Parliament.Model
{
    public class UnregisteredDocumentCreator : DocumentCreator
    {
        public enum UnregDCRole
        {
                President

            ,   PrimeMinister
            ,   VicePrimeMinister

            ,   MinisterOfInternalAffairs
            ,   MinisterOfJustice
            ,   MinisterOfFinances
            ,   MinisterOfEducation
            ,   MinisterOfHealth
            ,   MinisterOfSocialPolicy
            ,   MinisterOfEcology
            ,   MinisterOfCulture
            ,   MinisterOfForeignAffairs
            ,   MinisterOfAgrarianPolicy
            ,   MinisterOfFuelAndEnergy
            ,   MinisterOfYouthAndSports
            ,   MinisterOfInrastructure
            ,   MinisterOfDefence

            ,   PublicFigure
        }

/*------------------------------------------------------------------------------------------*/

        public UnregDCRole Role { get; set; }

        public string Name { get; set; }

/*------------------------------------------------------------------------------------------*/

        protected UnregisteredDocumentCreator () {}

/*------------------------------------------------------------------------------------------*/

        public UnregisteredDocumentCreator ( Guid _id, string _name, UnregDCRole _role )
            :   base ( _id )
        {
            Name = _name;
            Role = _role;
        }

/*------------------------------------------------------------------------------------------*/

        public override bool isRegisteredUser ()
        {
            return false;
        }

/*------------------------------------------------------------------------------------------*/

        public override Account getAccount ()
        {
            return null;
        }

/*------------------------------------------------------------------------------------------*/

        public override string getName ()
        {
            return Name;
        }
    }
}


/********************************************************************************************/