﻿/********************************************************************************************/
using System;
using System.IO;
/********************************************************************************************/


namespace Parliament.Model
{
    public class DocumentText
    {
        public string DocumentPath { get; set; }

/*------------------------------------------------------------------------------------------*/

        private DocumentText () {}

/*------------------------------------------------------------------------------------------*/

        public DocumentText ( string _path )
        {
            DocumentPath = Path.GetFullPath( _path );
        }

/*------------------------------------------------------------------------------------------*/

        private static void checkPath ( string _path )
        {
            if ( _path.Length == 0 )
                throw new ArgumentException( "No path to document text specified." );

            if ( !File.Exists( _path ) )
                throw new ArgumentNullException( "No document text file specified." );
        }

/*------------------------------------------------------------------------------------------*/

        public string asString()
        {
            string docText = "";

            try
            {
                using ( StreamReader sr = new StreamReader( File.Open(DocumentPath, FileMode.Open ) ) )
                {
                    docText = sr.ReadToEnd();
                }
            }

            catch ( Exception e )
            {
                throw new Exception( "The file you try to open could not be read.\n" + e.Message );
            }

            return docText;
        }
    }
}


/********************************************************************************************/