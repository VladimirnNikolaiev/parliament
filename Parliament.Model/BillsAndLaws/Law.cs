﻿/********************************************************************************************/
using System;
using Parliament.Exceptions;
/********************************************************************************************/


namespace Parliament.Model
{
    public class Law : Document
    {
        public enum LawsStatus
        {
                None = 0    /* Default value */

            ,   Removed
            ,   Act
        }

/*------------------------------------------------------------------------------------------*/

        public LawsStatus Status { get; set; }

/*------------------------------------------------------------------------------------------*/

        protected Law () {}

/*------------------------------------------------------------------------------------------*/

        public Law ( Guid _id, string _name, LawsStatus _status, Revision _revision )
            :   base ( _id, _name, _revision )
        {
            if( _status == LawsStatus.Removed )
                throw DocumentException.InvalidDocumentStatus( "Law", _id );

            Status = _status;
        }

/*------------------------------------------------------------------------------------------*/

        public override string ToString ()
        {
            return "Status = " + Status + base.ToString();
        }
    }
}


/********************************************************************************************/