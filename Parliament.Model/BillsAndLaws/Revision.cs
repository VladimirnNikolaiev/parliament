﻿/********************************************************************************************/
using System;
using System.Collections.Generic;

using Parliament.Exceptions;
/********************************************************************************************/


namespace Parliament.Model
{
    public class Revision : Utils.Entity
    {
        public DocumentInfo RevisionInfo { get; private set; }
        public DateTime CreationDate { get; private set; }

        public IList< DocumentCreator > Creators { get; private set; }

/*------------------------------------------------------------------------------------------*/

        public string DocumentText
        {
            get { return RevisionInfo.DocumentText; }
        }

/*------------------------------------------------------------------------------------------*/

        protected Revision () {}

/*------------------------------------------------------------------------------------------*/

        public Revision ( Guid _id, DateTime _creationDate, DocumentInfo _revisionInfo )
            :   base ( _id )
        {
            if ( _creationDate == null )
                throw RevisionException.InvalidCreationDate( _id );

            if ( _revisionInfo == null )
                throw RevisionException.EmptyRevisionInfo( _id );

            CreationDate = _creationDate;
            RevisionInfo = _revisionInfo;

            Creators = new List< DocumentCreator >();
        }

/*------------------------------------------------------------------------------------------*/

        public DocumentCreator MajorCreator
        {
            get { return Creators[ 0 ]; }
        }

/*------------------------------------------------------------------------------------------*/

        public int CreatorsCount
        {
            get { return Creators.Count; }
        }

/*------------------------------------------------------------------------------------------*/

        public DocumentCreator getCreator ( int _index )
        {
            return Creators[ _index ];
        }

/*------------------------------------------------------------------------------------------*/

        public void addCreator ( DocumentCreator _creator )
        {
            if ( _creator == null )
                throw new ArgumentNullException( "Empty Document Creator" );

            if ( Creators.Contains( _creator ) )
                throw RevisionException.SameDocumentCreator( _creator.DomainId );

            Creators.Add( _creator );
        }

 /*------------------------------------------------------------------------------------------*/

        public override string ToString ()
        {
            return string.Format (
                    "ID = {0}\nCreationDate = {1}\nRevisionInfo = {2} Creator = {3}"
                ,   DomainId
                ,   CreationDate.ToString( "MM/dd/yyyy" )
                ,   RevisionInfo.Document.DocumentPath
                ,   MajorCreator.ToString()
            );
        }
    }
}


/********************************************************************************************/