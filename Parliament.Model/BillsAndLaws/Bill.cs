﻿/********************************************************************************************/
using System;
using Parliament.Exceptions;
/********************************************************************************************/


namespace Parliament.Model
{
    public class Bill : Document
    {
        public enum BillsStatus
        {
                None = 0    /* Default value */

            ,   New
            ,   ComesIntoForce
            ,   AwaitsPresidentsSignature
            ,   Vetoed
            ,   VotedAsAWhole
            ,   VotedIn1stReading
            ,   VotedIn2ndReading
            ,   VotedIn3rdReading
            ,   IncludedToAgenda
        }

/*------------------------------------------------------------------------------------------*/

        public BillsStatus Status { get; set; }

/*------------------------------------------------------------------------------------------*/

        protected Bill () {}

/*------------------------------------------------------------------------------------------*/

        public Bill ( Guid _id, string _name, BillsStatus _status, Revision _revision )
            :   base ( _id, _name, _revision )
        {
            if( _status == BillsStatus.Vetoed )
                throw DocumentException.InvalidDocumentStatus( "Bill", _id );

            Status = _status;
        }

/*------------------------------------------------------------------------------------------*/

        public override string ToString ()
        {
            return "Status = " + Status + base.ToString();
        }
    }
}


/********************************************************************************************/