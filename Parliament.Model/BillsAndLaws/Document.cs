﻿/********************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;

using Parliament.Exceptions;
/********************************************************************************************/


namespace Parliament.Model
{
    public abstract class Document : Utils.Entity
    {
        public string Name { get; set; }

        public IList< Revision > Revisions { get; private set; }

 /*------------------------------------------------------------------------------------------*/

        public void addRevision ( Revision _revision )
        {
            Revisions.Add( _revision );
        }

/*------------------------------------------------------------------------------------------*/

        public int RevisionsCount
        {
            get { return Revisions.Count; }
        }

/*------------------------------------------------------------------------------------------*/

        public Revision FinalRevision
        {
            get { return Revisions.Last(); }
        }

/*------------------------------------------------------------------------------------------*/

        public Revision getRevision ( int _index )
        {
            return Revisions[ _index ];
        }

/*------------------------------------------------------------------------------------------*/

        public int getRevisionId ( Revision _revison )
        {
            return Revisions.IndexOf( _revison );
        }

 /*------------------------------------------------------------------------------------------*/

        public DateTime CreationDate
        {
            get { return Revisions[ 0 ].CreationDate; }
        }

/*------------------------------------------------------------------------------------------*/

        public string CreationDateAsString
        {
            get { return CreationDate.ToString( "MM/dd/yyyy" ); }
        }

/*------------------------------------------------------------------------------------------*/

        public string DocumentText
        {
            get { return FinalRevision.DocumentText; }
        }

/*------------------------------------------------------------------------------------------*/

        protected Document () {}

/*------------------------------------------------------------------------------------------*/

        protected Document (Guid _id, string _name, Revision _revision )      
            :   base ( _id )
        {
            Name = _name;
            Revisions = new List< Revision >();

            Revisions.Add( _revision );
        }

/*------------------------------------------------------------------------------------------*/

        public void updateDocument ( Revision _revision )
        {
            if( Revisions.Contains( _revision ) )
                throw RevisionException.SameRevision( _revision.DomainId );

           addRevision( _revision );
        }

/*------------------------------------------------------------------------------------------*/

        public override string ToString ()
        {
            return string.Format (
                    "\nID = {0}\nName = {1}\nCreationDate = {2}\nDocumentText:\n{3}"
                ,   DomainId
                ,   Name
                ,   CreationDate.ToString( "MM/dd/yyyy" )                                 //  27/05/1996 
                ,   DocumentText
            ); 
        }
    }
}


/********************************************************************************************/