﻿/********************************************************************************************/
using System;
using System.Diagnostics.Tracing;

using Microsoft.Practices.EnterpriseLibrary.SemanticLogging;
using Microsoft.Practices.Unity;
/********************************************************************************************/

namespace Parliament.Dependencies
{
    public class LogListener : IDisposable
    {

        internal void OnStartup ()
        {
            listenerInfo = FlatFileLog.CreateListener( "parliament_services.log" );
            listenerInfo.EnableEvents( Log, EventLevel.LogAlways, ParliamentEventSource.Keywords.ServiceTracing );

            listenerErrors = FlatFileLog.CreateListener( "parliament_diagnostic.log" );
            listenerErrors.EnableEvents( Log, EventLevel.LogAlways, ParliamentEventSource.Keywords.Diagnostic );

            Log.StartupSucceeded();
        }

/*------------------------------------------------------------------------------------------*/

        public void Dispose ()
        {
            listenerInfo.DisableEvents( Log );
            listenerErrors.DisableEvents( Log );

            listenerInfo.Dispose();
            listenerErrors.Dispose();
        }

/*------------------------------------------------------------------------------------------*/

        [ Dependency ]
        protected ParliamentEventSource Log { get; set; }

        private EventListener listenerInfo;
        private EventListener listenerErrors;
    }
}
