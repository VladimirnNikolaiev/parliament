﻿/********************************************************************************************/
using System;
using System.Linq;
using System.Reflection;

using Microsoft.Practices.Unity;
using Microsoft.Practices.Unity.InterceptionExtension;
using Microsoft.Practices.EnterpriseLibrary.Validation.PolicyInjection;

using Parliament.Repository.EntityFramework;
/********************************************************************************************/

namespace Parliament.Dependencies
{
    public static class ContainerBootstraper
    {
        public static void RegisterTypes ( IUnityContainer _container, ParliamentDbContext _dbContext )
        {
            _container.AddNewExtension< Interception >();

            _container.RegisterInstance< ParliamentDbContext >( _dbContext );

            RegisterLogFacilities( _container );
            RegisterServices( _container );
            RegisterRepositories( _container );

            // NOTE: For debug purposes.
            //PrintContainerDebuggingInfo( _container );
        }

/*------------------------------------------------------------------------------------------*/

        private static void RegisterLogFacilities ( IUnityContainer container )
        {
            container.RegisterInstance< ParliamentEventSource >( new ParliamentEventSource() );
            container.RegisterType( 
                    typeof( LogListener )
                ,   new ContainerControlledLifetimeManager() 
            );

            var logListener = container.Resolve< LogListener >();
            logListener.OnStartup();
        }

/*------------------------------------------------------------------------------------------*/

        private static void RegisterServices( IUnityContainer _container )
        {
            _container.Configure< Interception >()
                .AddPolicy( "ValidationPolicy" )
                .AddMatchingRule< NamespaceMatchingRule >(
                      new InjectionConstructor( "Parliament.Service.Impl", true )
                )
                .AddCallHandler(
                    new ValidationCallHandler( "", SpecificationSource.Both )
                )
            ;

            _container.RegisterTypes(
                    AllClasses.FromAssemblies(
                        new Assembly[] 
                        {
                            Assembly.Load( "Parliament.Service.Impl" )
                        }
                    )

                ,   WithMappings.FromMatchingInterface
                ,   WithName.Default
                ,   WithLifetime.ContainerControlled
                ,   getInjectionMembers: t => new InjectionMember[]
                    {
                        new Interceptor< InterfaceInterceptor >(),
                        new InterceptionBehavior< ExceptionInterceptionBehavior >(),
                        new InterceptionBehavior< SemanticLoggingInterceptionBehavior >(),
                        new InterceptionBehavior< TransactionInterceptionBehavior< ParliamentDbContext > >(),
                        new InterceptionBehavior< PolicyInjectionBehavior >( "ValidationPolicy" )
                    }
            );
        }

/*------------------------------------------------------------------------------------------*/

        private static void RegisterRepositories ( IUnityContainer _container )
        {
            _container.RegisterTypes(
                    AllClasses.FromAssemblies(
                        new Assembly[] 
                        {
                            Assembly.Load( "Parliament.Repository.EntityFramework" )
                        }
                    ).Where( t => t != typeof( ParliamentDbContext ) )

                ,   WithMappings.FromMatchingInterface
                ,   WithName.Default
                ,   WithLifetime.ContainerControlled
            );
        }

/*------------------------------------------------------------------------------------------*/

        private static void PrintContainerDebuggingInfo ( IUnityContainer _container )
        {
            Console.WriteLine( "Container has {0} Registrations:", _container.Registrations.Count() );
            foreach ( ContainerRegistration item in _container.Registrations )
                System.Console.WriteLine( item.GetMappingAsString() );
        }

/*------------------------------------------------------------------------------------------*/

        private static string GetMappingAsString ( this ContainerRegistration _registration )
        {
            string regName, regType, mapTo, lifetime;

            var r = _registration.RegisteredType;
            regType = r.Name + GetGenericArgumentsList( r );

            var m = _registration.MappedToType;
            mapTo = m.Name + GetGenericArgumentsList( m );

            regName = _registration.Name ?? "[default]";

            lifetime = _registration.LifetimeManagerType.Name;
            if ( mapTo != regType )
                mapTo = " -> " + mapTo;
            else
                mapTo = string.Empty;

            lifetime = lifetime.Substring( 0, lifetime.Length - "LifetimeManager".Length );
            return String.Format( "+ {0}{1}  '{2}'  {3}", regType, mapTo, regName, lifetime );
        }

/*------------------------------------------------------------------------------------------*/

        private static string GetGenericArgumentsList ( Type _type )
        {
            if (_type.GetGenericArguments().Length == 0 )
                return string.Empty;

            string arglist = string.Empty;
            bool first = true;
            foreach ( Type t in _type.GetGenericArguments() )
            {
                arglist += first ? t.Name : ", " + t.Name;
                first = false;
                if ( t.GetGenericArguments().Length > 0 )
                    arglist += GetGenericArgumentsList( t );
            }
            return "<" + arglist + ">";
        }
    }
}


/********************************************************************************************/