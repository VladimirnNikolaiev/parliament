﻿/********************************************************************************************/
using System;
using System.Collections.Generic;
/********************************************************************************************/


namespace Parliament.Dto
{
    public abstract class DomainEntityDto
    {
        public Guid DomainId { get; private set; }

/*------------------------------------------------------------------------------------------*/

        protected DomainEntityDto ( Guid _domainId )
        {
            this.DomainId = _domainId;
        }

/*------------------------------------------------------------------------------------------*/

        protected abstract IEnumerable< object > GetAttributesToIncludeInEqualityCheck ();
    }
}


/********************************************************************************************/