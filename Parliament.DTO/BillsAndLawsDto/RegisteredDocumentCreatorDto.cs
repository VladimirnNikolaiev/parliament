﻿/********************************************************************************************/
using System;
using System.Collections.Generic;
/********************************************************************************************/


namespace Parliament.Dto
{
    public class RegisteredDocumentCreatorDto : DocumentCreatorDto
    {
        public UserDto User { get; private set; }

/*------------------------------------------------------------------------------------------*/

        public RegisteredDocumentCreatorDto (
                Guid _domainId
            ,   UserDto _user
            ,   IList< Guid > _documents
        )
            :   base ( _domainId, _documents )
        {
            User = _user;
        }

/*------------------------------------------------------------------------------------------*/

        // NOTE: In case something doesn't work pay attention on this method.
        // Not sure about correctness of cast.

        protected override IEnumerable< object > GetAttributesToIncludeInEqualityCheck ()
        {
            IList< object > attributesList = ( IList< object > )base.GetAttributesToIncludeInEqualityCheck();

            attributesList.Add( User.DomainId );

            return attributesList;
        }

/*------------------------------------------------------------------------------------------*/

        public override string ToString ()
        {
            return string.Format(
                    "{1}\nUserId: {2}\n"
                ,   base.ToString()
                ,   User.DomainId
            );
        }
    }
}


/********************************************************************************************/