﻿/********************************************************************************************/
using System;
using System.Collections.Generic;
/********************************************************************************************/


namespace Parliament.Dto
{
    public class RevisionDto : DomainEntityDto
    {
        public string DocumentTextFilePath { get; private set; }
        public DateTime CreationDate { get; private set; }
        public IList< Guid > Creators { get; private set; }

/*------------------------------------------------------------------------------------------*/

        public RevisionDto ( 
                Guid _domainId
            ,   string _documentTextFilePath
            ,   DateTime _creationDate
            ,   IList< Guid > _creators 
        )
            :   base ( _domainId )
        {
            this.DocumentTextFilePath = _documentTextFilePath;
            this.CreationDate = _creationDate;
            this.Creators = _creators;
        }

/*------------------------------------------------------------------------------------------*/

        protected override IEnumerable< object > GetAttributesToIncludeInEqualityCheck ()
        {
            return new List< object > { base.DomainId, CreationDate, Creators.Count };
        }

/*------------------------------------------------------------------------------------------*/

        public override string ToString ()
        {
            return string.Format(
                    "RevisionId: {0}, CreationDate: {1}\nDocumentTextFilePath:\n {2}\n"
                ,   base.DomainId
                ,   CreationDate
                ,   DocumentTextFilePath    
            );
        }
    }
}


/********************************************************************************************/