﻿/********************************************************************************************/
using System;
using System.Collections.Generic;
/********************************************************************************************/


namespace Parliament.Dto
{
    public class LawDto : DocumentDto
    {
        public string Status { get; private set; }
        
/*------------------------------------------------------------------------------------------*/

        public LawDto ( Guid _domainId, string _name, string _status, IList< RevisionDto > _revisions )
            :   base( _domainId, _name, _revisions )
        {
            this.Status = _status;
        }

/*------------------------------------------------------------------------------------------*/

        // NOTE: In case something doesn't work pay attention on this method.
        // Not sure about correctness of cast.

        protected override IEnumerable< object > GetAttributesToIncludeInEqualityCheck ()
        {
            IList< object > attributesList = ( IList< object >)base.GetAttributesToIncludeInEqualityCheck();

            attributesList.Add( Status );

            return attributesList;
        }

/*------------------------------------------------------------------------------------------*/

        public override string ToString ()
        {
            return "Status: " + Status + base.ToString();
        }
    }
}


/********************************************************************************************/