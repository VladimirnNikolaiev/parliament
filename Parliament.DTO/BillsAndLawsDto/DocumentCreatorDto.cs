﻿/********************************************************************************************/
using System;
using System.Collections.Generic;
/********************************************************************************************/

namespace Parliament.Dto
{
    public abstract class DocumentCreatorDto : DomainEntityDto
    {  
        public IList < Guid > Documents { get; private set; }

/*------------------------------------------------------------------------------------------*/

        protected DocumentCreatorDto ( Guid _domainId, IList< Guid > _documents )
            :   base ( _domainId )
        {
            this.Documents = _documents;
        }

/*------------------------------------------------------------------------------------------*/

        public int CreatedDocumentCount
        {
            get { return Documents.Count; }
        }

/*------------------------------------------------------------------------------------------*/

        protected override IEnumerable< object > GetAttributesToIncludeInEqualityCheck ()
        {
            return new List< object > { base.DomainId };
        }

/*------------------------------------------------------------------------------------------*/

        public override string ToString ()
        {
            return string.Format(
                    "Amount of created docs: {1}\n"
                ,   CreatedDocumentCount 
            );
        }
    }
}


/********************************************************************************************/