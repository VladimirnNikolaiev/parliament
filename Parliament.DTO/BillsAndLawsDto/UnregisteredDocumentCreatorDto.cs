﻿/********************************************************************************************/
using System;
using System.Collections.Generic;
/********************************************************************************************/


namespace Parliament.Dto
{
    public class UnregisteredDocumentCreatorDto : DocumentCreatorDto
    {
        public string Role { get; private set; }
        public string Name { get; private set; } 

/*------------------------------------------------------------------------------------------*/

        public UnregisteredDocumentCreatorDto ( 
                Guid _domainId
            ,   string _name
            ,   string _role
            ,   IList< Guid > _documents 
        )
            :   base ( _domainId, _documents )
        {
            this.Role = _role;
            Name = _name;
        }

/*------------------------------------------------------------------------------------------*/

        // NOTE: In case something doesn't work pay attention on this method.
        // Not sure about correctness of cast.

        protected override IEnumerable< object > GetAttributesToIncludeInEqualityCheck ()
        {
            IList< object > attributesList = ( IList< object > )base.GetAttributesToIncludeInEqualityCheck();

            attributesList.Add(Name);
            attributesList.Add( Role );

            return attributesList;
        }

/*------------------------------------------------------------------------------------------*/

        public override string ToString ()
        {
            return string.Format(
                    "{1}\nName: {2}\nRole: {3}\n"
                ,   base.ToString()
                ,   Name
                ,   Role
            );
        }
    }
}


/********************************************************************************************/