﻿/********************************************************************************************/
using System;
using System.Collections.Generic;
/********************************************************************************************/


namespace Parliament.Dto
{
    public abstract class DocumentDto : DomainEntityDto
    {
        public string Name { get; private set; }
        public IList< RevisionDto > Revisions { get; private set; }

/*------------------------------------------------------------------------------------------*/

        protected DocumentDto ( Guid _domainId, string _name, IList< RevisionDto > _revisions )
            :   base ( _domainId )
        {
            this.Name = _name;
            this.Revisions = _revisions;
        }

/*------------------------------------------------------------------------------------------*/

        private RevisionDto FinalRevision
        {
            get
            {
                // NOTE: Somehow there isn't any Last() method.
                return Revisions[ Revisions.Count - 1 ];
            }
        }

/*------------------------------------------------------------------------------------------*/

        private string DocumentTextFilePath
        {
            get { return FinalRevision.DocumentTextFilePath; }
        }

/*------------------------------------------------------------------------------------------*/

        private DateTime CreationDate
        {
            get { return Revisions[0].CreationDate; }
        }

/*------------------------------------------------------------------------------------------*/

        protected override IEnumerable< object > GetAttributesToIncludeInEqualityCheck ()
        {
            return new List< object > { base.DomainId, Name, CreationDate, FinalRevision.DomainId };
        }

/*------------------------------------------------------------------------------------------*/

        public override string ToString ()
        {
            return string.Format(
                    "DocumentId: {0}, Name: {1}, CreationDate: {2}\nDocumentTextFilePath:\n{3}"
                ,   base.DomainId
                ,   Name
                ,   CreationDate.ToString("MM/dd/yyyy")    
                ,   DocumentTextFilePath
            );
        }
    }
}


/********************************************************************************************/