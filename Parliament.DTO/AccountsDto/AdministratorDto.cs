﻿/********************************************************************************************/
using System;
using System.Collections.Generic;
/********************************************************************************************/


namespace Parliament.Dto
{
    public class AdministratorDto : AccountDto
    {
        public string AccessRight { get; private set; }

/*------------------------------------------------------------------------------------------*/

        public AdministratorDto ( 
                Guid _domainId
            ,   string _name
            ,   string _email
            ,   string _accessRight
            ,   string _status 
            ,   string _imageUrl
        )
            : base ( _domainId, _name, _email, _status, _imageUrl )
        {
            this.AccessRight = _accessRight;
        }

/*------------------------------------------------------------------------------------------*/

        // NOTE: In case something doesn't work pay attention on this method.
        // Not sure about correctness of cast.

        protected override IEnumerable< object > GetAttributesToIncludeInEqualityCheck ()
        {
            IList< object > attributesList = ( IList< object > )base.GetAttributesToIncludeInEqualityCheck() ;

            attributesList.Add( AccessRight );

            return attributesList;
        }

/*------------------------------------------------------------------------------------------*/

        public override string ToString ()
        {
            return "AccessRight: " + AccessRight + base.ToString();
        }
    }
}


/********************************************************************************************/