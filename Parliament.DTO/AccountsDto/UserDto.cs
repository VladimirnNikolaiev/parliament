﻿/********************************************************************************************/
using System;
using System.Collections.Generic;
/********************************************************************************************/


namespace Parliament.Dto
{
    public class UserDto : AccountDto
    {
        public string UserPost { get; private set; }

/*------------------------------------------------------------------------------------------*/

        public UserDto ( 
                Guid _domainId
            ,   string _name
            ,   string _email
            ,   string _userPost
            ,   string _status 
            ,   string _imageUrl    
        )
            :   base ( _domainId, _name, _email, _status, _imageUrl )
        {
            this.UserPost = _userPost;
        }

/*------------------------------------------------------------------------------------------*/

        // NOTE: In case something doesn't work pay attention on this method.
        // Not sure about correctness of cast.

        protected override IEnumerable < object > GetAttributesToIncludeInEqualityCheck ()
        {
            IList< object > attributesList = ( IList< object > )base.GetAttributesToIncludeInEqualityCheck();

            attributesList.Add( UserPost );

            return attributesList;
        }

/*------------------------------------------------------------------------------------------*/

        public override string ToString ()
        {
            return "Post: " + UserPost + base.ToString(); 
        }
    }
}


/********************************************************************************************/