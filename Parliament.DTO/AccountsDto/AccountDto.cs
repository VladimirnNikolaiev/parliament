﻿/********************************************************************************************/
using System;
using System.Collections.Generic;
/********************************************************************************************/


namespace Parliament.Dto
{
    public abstract class AccountDto : DomainEntityDto
    {
        public string Name { get; private set; }
        public string Email { get; private set; }
        public string Status { get; private set; }
        public string ImageUrl { get; private set; }

/*------------------------------------------------------------------------------------------*/

        public AccountDto ( Guid _domainId, string _name, string _email, string _status, string _imageUrl )
            :   base ( _domainId )
        {
            this.Name = _name;
            this.Email = _email;
            this.Status = _status;
            this.ImageUrl = _imageUrl;
        }

/*------------------------------------------------------------------------------------------*/

        protected override IEnumerable< object > GetAttributesToIncludeInEqualityCheck ()
        {
            return new List< object > { base.DomainId, Name, Email };
        }

/*------------------------------------------------------------------------------------------*/

        public override string ToString ()
        {
            return string.Format(
                    "\nAccount Id: {0}, Name: {1}, Email: {2}, ImageUrl: {3}\n"
                ,   base.DomainId
                ,   Name
                ,   Email
                ,   ImageUrl
            );
        }
    }
}


/********************************************************************************************/
